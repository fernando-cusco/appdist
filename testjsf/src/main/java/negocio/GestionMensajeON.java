package negocio;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import testjsf.modelo.Mensaje;


@Stateless
public class GestionMensajeON  implements GestionMensajesRemoto, GestionMensajesLocal{

	
	private List<Mensaje> mensajes = new ArrayList<Mensaje>();
	
	public void guardar(Mensaje msj){
		
		System.out.println(msj.getNombre());
		System.out.println(msj.getEmail());
		System.out.println(msj.getMensaje());
		this.mensajes.add(msj);
	}
	
	public List<Mensaje> getMensajes() {
		return mensajes;
	}
	
	public int nuevoCodigo() {
		return mensajes.size() + 1;
	}
}
