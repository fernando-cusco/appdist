package negocio;

import java.util.List;

import javax.ejb.Remote;

import testjsf.modelo.Mensaje;

@Remote
public interface GestionMensajesRemoto {

	
	public void guardar(Mensaje msj);
	public List<Mensaje> getMensajes();
	public int nuevoCodigo();
}
