package negocio;

import java.util.List;

import javax.ejb.Local;

import testjsf.modelo.Mensaje;

@Local
public interface GestionMensajesLocal {

	
	public void guardar(Mensaje msj);
	public List<Mensaje> getMensajes();
	
	public int nuevoCodigo();
}