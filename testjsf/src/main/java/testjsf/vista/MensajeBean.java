package testjsf.vista;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import negocio.GestionMensajeON;
import negocio.GestionMensajesLocal;
import testjsf.modelo.Mensaje;

@ManagedBean
public class MensajeBean {
	//ejb no se puede instanciar , usamos Inject
	@Inject
	private GestionMensajesLocal gestion;
	
	@Inject
	private FacesContext facesContext;

	
	
	/*Bean Properties*/
	private Mensaje msg = new Mensaje();
	private List<Mensaje> mensajes;
	
	//ejecuta cuando se ejcuta la pagina 
	@PostConstruct
	public void init() {
		msg.setCodigo(gestion.nuevoCodigo());
	}
	
	
	public List<Mensaje> getMensajes() {
		return mensajes;
	}
	public void setMensajes(List<Mensaje> mensajes) {
		this.mensajes = mensajes;
	}
	public Mensaje getMsg() {
		return msg;
	}
	public void setMsg(Mensaje msg) {
		this.msg = msg;
	}
	public String enviarMensaje() {
		try {
			gestion.guardar(msg);
			mensajes = gestion.getMensajes();
		} catch (Exception e) {
			String errores = e.getMessage();
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, errores, "Registration unsuccessful");
			facesContext.addMessage(null, m);
			return null;
		}
		return "listado";
	}
	
}
