package testjsf.modelo;

import javax.validation.constraints.NotNull;

public class Mensaje {

	@NotNull(message = "Error de validacion")
	private String nombre;
	@NotNull(message = "Error de validacion")
	private String email;
	@NotNull(message = "Error de validacion")
	private String mensaje;
	
	private int codigo;
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	
}
