package webservice;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.QueryParam;

import modelo.Cliente;
import negocio.ClienteON;
import negocio.RecargaON;

@WebService
public class ServiceSoap {

	
	@Inject
	private RecargaON on;
	
	@Inject
	private ClienteON onn;
	
	@WebMethod
	public void nuevaRecarga(Transaccion transaccion) {
		on.nuevaRecarga(transaccion);
	}
	
	
	@WebMethod
	public Cliente misRecargas(@QueryParam("cedula") String cedula) {
		return onn.misRecargas(cedula);
	}
}
