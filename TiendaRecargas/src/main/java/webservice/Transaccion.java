package webservice;

public class Transaccion {

	private String telefono;

	private Double monto;

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	@Override
	public String toString() {
		return "Transaccion [telefono=" + telefono + ", monto=" + monto + "]";
	}

}
