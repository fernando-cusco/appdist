package webservice;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import modelo.Cliente;
import modelo.Recarga;
import negocio.ClienteON;
import negocio.RecargaON;

@Path("/cliente")
public class ServiceRest {

	@Inject
	private ClienteON on;
	
	@Inject
	private RecargaON onr;
	
	@POST
	@Path("/nuevo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void nuevoCliente(Cliente cliente) {
		on.crearCliente(cliente);
	}
	
	@GET
	@Path("/recargas")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Cliente misRecargas(@QueryParam("cedula") String cedula) {
		return on.misRecargas(cedula);
	}
	
	@POST
	@Path("/recargar")
	@Consumes(MediaType.APPLICATION_JSON)
	public void nuevaRecarga(Transaccion transaccion) {
		onr.nuevaRecarga(transaccion);
	}
	
}
