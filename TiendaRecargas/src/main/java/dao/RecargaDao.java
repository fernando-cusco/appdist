package dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import modelo.Recarga;

public class RecargaDao {

	@Inject
	private EntityManager em;

	public void nuevaRecarga(Recarga recarga) {
		em.persist(recarga);
	}

}
