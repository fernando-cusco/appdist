package dao;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import modelo.Cliente;
import modelo.Recarga;

public class ClienteDao {

	@Inject
	private EntityManager em;
	
	public void nuevoCliente(Cliente cliente) {
		em.persist(cliente);
	}
	
	
	public Cliente misRecargas(String cedula) {
		String jqpl = "SELECT c FROM Cliente c JOIN FETCH c.recargas where c.cedula = :cedula";
		Query query = em.createQuery(jqpl, Cliente.class);
		query.setParameter("cedula", cedula);
		Cliente cli = (Cliente) query.getSingleResult();
		List<Recarga> recargas = new ArrayList<>();
		for (Recarga recarga : cli.getRecargas()) {
			recargas.add(recarga);
		}
		return cli;
	}
	
	public Cliente buscar(String telefono) {
		String jpql = "SELECT c FROM Cliente c where c.telefono = :telefono";
		Query query = em.createQuery(jpql, Cliente.class);
		query.setParameter("telefono", telefono);
		Cliente cliente = (Cliente) query.getSingleResult();
		return cliente;
	}
	
	public Cliente actualizar(Cliente c) {
		return (Cliente) em.merge(c);
	}
	
}
