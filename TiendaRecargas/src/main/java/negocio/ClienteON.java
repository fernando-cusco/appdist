package negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import dao.ClienteDao;
import modelo.Cliente;
import modelo.Recarga;

@Stateless
public class ClienteON {

	@Inject
	private ClienteDao dao;
	
	public void crearCliente(Cliente cliente) {
		dao.nuevoCliente(cliente);
	}
	
	public Cliente misRecargas(String cedula) {
		Cliente c = null;
		try {
			c = dao.misRecargas(cedula);
		} catch (Exception e) {
			System.out.println(e.getMessage()+" Error");
		}
		return c;
	}
}
