package negocio;

import java.util.Date;

import javax.ejb.Stateless;
import javax.inject.Inject;

import dao.ClienteDao;
import dao.RecargaDao;
import modelo.Cliente;
import modelo.Recarga;
import webservice.Transaccion;

@Stateless
public class RecargaON {

	@Inject
	private RecargaDao dao;
	
	@Inject
	private ClienteDao cli;
	
	
	public void nuevaRecarga(Transaccion transaccion) {
		Cliente cliente = cli.buscar(transaccion.getTelefono());
		cliente.setSaldo(cliente.getSaldo()+transaccion.getMonto());
		Recarga recarga = new Recarga();
		recarga.setFecha(new Date());
		recarga.setMonto(transaccion.getMonto());
		recarga.setTelefono(transaccion.getTelefono());
		cliente.nuevaRecarga(recarga);
		dao.nuevaRecarga(recarga);
	}
	
}
