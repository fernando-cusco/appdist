package modelo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Recarga> recargas;

	private String cedula;

	private String nombres;

	private String telefono;

	private Double saldo;

	public void nuevaRecarga(Recarga recarga) {
		if (recargas == null) {
			recargas = new ArrayList<Recarga>();
		}
		recargas.add(recarga);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public List<Recarga> getRecargas() {
		return recargas;
	}

	public void setRecargas(List<Recarga> recargas) {
		this.recargas = recargas;
	}

	@Override
	public String toString() {
		return "Cliente [id=" + id + ", recargas=" + recargas + ", cedula=" + cedula + ", nombres=" + nombres
				+ ", telefono=" + telefono + ", saldo=" + saldo + "]";
	}

}
