package dao;


import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import modelo.Author;
import modelo.Book;

@Stateless
public class BookAuthor {
	
	@Inject
	private EntityManager em;
	
	public void insertar() {
		/*Creamos los autores*/
		Author a1 = new Author();
		a1.setName("Autor 1");
		
		Author a2 = new Author();
		a2.setName("Autor 2");
		
		Author a3 = new Author();
		a3.setName("Autor 3");
		
		
		/*Creamos los libros*/
		Book b1 = new Book();
		b1.setName("Libro 1 - El lobo");
		b1.addAuthor(a1);
		b1.addAuthor(a3);
		
		Book b2 = new Book();
		b2.setName("Libro 2 - El conejo");
		b2.addAuthor(a2);
		b2.addAuthor(a3);
		
		em.persist(b1);
		em.persist(b2);
		
	}
	
	
	
	
}
