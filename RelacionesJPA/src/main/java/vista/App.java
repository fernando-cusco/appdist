package vista;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import dao.BookAuthor;

@ManagedBean
public class App {

	@Inject
	private BookAuthor ba;
	
	
	public void run() {
		ba.insertar();
	}
}
