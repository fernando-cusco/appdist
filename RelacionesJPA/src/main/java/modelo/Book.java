package modelo;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "books")
public class Book {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name", nullable = false)
	private String name;
	
	
	@JoinTable(
			name = "books_authors",
			joinColumns = @JoinColumn(name = "FK_BOOK", nullable = false),
			inverseJoinColumns = @JoinColumn(name = "FK_AUTHOR", nullable = false)
			)
	@ManyToMany(cascade = CascadeType.ALL)
	private List<Author> authors;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}

	public void addAuthor(Author a) {
		if (this.authors == null) {
			this.authors = new ArrayList<Author>();
		}
		this.authors.add(a);
	}
	
	
}
