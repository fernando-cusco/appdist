package servicios;

import java.util.List;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import modelo.Libro;
import negocio.GestionLibrosLocal;

@WebService
public class LibrosServiceSoap {

	@Inject
	private GestionLibrosLocal gl;
	
	
	@WebMethod
	public List<Libro> getLibros(){
		
		
		return gl.getLibros();
	}
	
	
	@WebMethod
	public Respuesta createLibro(Libro libro) {
		Respuesta r = new Respuesta();
		try {
			
			gl.guardarLibro(libro.getCodigo(), libro.getTitulo());
			r.setCodigo(0);
			r.setMensaje("OK");
		}catch(Exception e) {
			r.setCodigo(99);
			r.setMensaje("Error al insertar");
		}
		return r;
	}
	
}
