package servicios;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import modelo.Libro;
import negocio.GestionLibrosLocal;

@Path("/libros")
public class LibrosService {
	
	@Inject
	private GestionLibrosLocal gl;
	
	
	@GET
	@Path("consultar")
	@Produces("application/json")
	public List<Libro> getLibros(){
		
		
		return gl.getLibros();
	}
	
	
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	public Respuesta createLibro(Libro libro) {
		Respuesta r = new Respuesta();
		try {
			
			gl.guardarLibro(libro.getCodigo(), libro.getTitulo());
			r.setCodigo(0);
			r.setMensaje("OK");
		}catch(Exception e) {
			r.setCodigo(99);
			r.setMensaje("Error al insertar");
		}
		return r;
	}

}
