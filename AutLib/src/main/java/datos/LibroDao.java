package datos;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import modelo.Libro;

@Stateless
public class LibroDao {

	@Inject
	private EntityManager em;
	
	public void crearLibro(Libro libro) {
		em.persist(libro);
	}
	
	public Libro read(int id) {
		return em.find(Libro.class, id);
	}
	
	public List<Libro> getLibrosAutor(){
		String jpql = "SELECT l FROM Libro l ";
		
		Query q = em.createQuery(jpql, Libro.class);
		
		List<Libro> libros = q.getResultList();
		
		return libros;
	}
	
	public List<Libro> getLibrosTipo(){
		String jpql = "SELECT l FROM Libro l ";
		
		Query q = em.createQuery(jpql, Libro.class);
		
		List<Libro> libros = q.getResultList();
		for (Libro libro : libros) {
		libro.getLibroAutor().size();
		}
		
		return libros;
	}
}
