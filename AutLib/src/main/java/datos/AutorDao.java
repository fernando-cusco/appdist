package datos;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import modelo.Autor;

@Stateless
public class AutorDao {

	@Inject
	private EntityManager em;
	
	public void crearAutor(Autor autor) {
		em.persist(autor);
	}
	
	
	public Autor buscar(int id) {
		return em.find(Autor.class, id);
	}
	
	public List<Autor> autores() {
		String jpql = "SELECT a FROM Autor a";
		Query query = em.createQuery(jpql, Autor.class);
		List<Autor> autors = query.getResultList();
		return autors;
	}
	
}
