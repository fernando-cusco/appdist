package vista;



import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import modelo.Autor;
import modelo.Libro;
import modelo.LibroAutor;
import negocio.AutorON;



@ManagedBean
@ViewScoped
public class AutorMB {

	private List<Libro> libros;
	private List<Autor> autores;
	
	@Inject
	private AutorON on;
	
	private Libro libro;
	
	@PostConstruct
	public void init() {
		libro = new Libro();
		libros = on.libros();
		autores = on.getListadoAutorLibros();
	}
	
	
	
	public String guardarLibro() {
		try {
			on.crearLibro(libro);
			libro = null;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	public String agregarAutor() {
		libro.addLibroAutor(new LibroAutor());
		return null;
	}
	
	public String buscarAutor(LibroAutor la) {
		try {
			Autor autor = on.buscar(la.getIdBuscar());
			la.setAutor(autor);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	public List<Libro> getLibros() {
		return libros;
	}

	public void setLibros(List<Libro> libros) {
		this.libros = libros;
	}

	public List<Autor> getAutores() {
		return autores;
	}

	public void setAutores(List<Autor> autores) {
		this.autores = autores;
	}



	public Libro getLibro() {
		return libro;
	}



	public void setLibro(Libro libro) {
		this.libro = libro;
	}

	
	
	
	
}
