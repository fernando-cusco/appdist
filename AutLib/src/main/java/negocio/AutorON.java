package negocio;

import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.inject.Inject;

import datos.AutorDao;
import datos.LibroDao;
import modelo.Autor;
import modelo.Libro;

@Singleton
public class AutorON {

	@Inject
	private AutorDao dao;
	
	@Inject
	private LibroDao daoL;
	
	
	public void crearLibro(Libro libro) {
		daoL.crearLibro(libro);
	}
	
	public Autor buscar(int codigo) throws Exception{
		try {
			Autor a = dao.buscar(codigo);
			return a;
		} catch (Exception e) {
			throw new Exception("Error");
		}
	}
	
	public List<Libro> libros() {
		return daoL.getLibrosAutor();
	}
	public List<Autor> getListadoAutorLibros(){
		return dao.autores();
	}
}
