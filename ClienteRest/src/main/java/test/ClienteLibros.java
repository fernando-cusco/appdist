package test;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;

public class ClienteLibros {
	
	public String WS_GET_LIBROS = "http://localhost:8080/LibrosJee/rest/libros";
	public String WS_GET_LIBROS1 = "http://localhost:8080/LibrosJee/rest/libros/buscar";
	
	public List<Libro> getLibros() {
		Client client = ClientBuilder.newClient();		
		WebTarget target = client.target(WS_GET_LIBROS);

		List<Libro> libros = target.request().get(new GenericType<List<Libro>>() {});
		client.close();
		
		return libros;		
	}
	
	//http://localhost:8080/LibrosJee/rest/libros?id=1
	public Libro buscar(int id) {
		Client client = ClientBuilder.newClient();		
		WebTarget target = client.target(WS_GET_LIBROS1).queryParam("id", id);
		Libro libro = target.request().get(Libro.class);
		client.close();
		return libro;
	}
}
