package negocio;

import javax.ejb.Stateless;
import javax.inject.Inject;

import dao.ProductoDao;
import modelos.Producto;
import utils.Pedido;
import utils.Respuesta;

@Stateless
public class ProductoON {

	@Inject
	private ProductoDao dao;
	
	public Respuesta nuevoProducto(Producto producto) {
		Respuesta respuesta = new Respuesta();
		try {
			dao.nuevoProducto(producto);
			respuesta.setCodigo(1);
			respuesta.setMensaje("producto agregado");
		} catch (Exception e) {
			respuesta.setCodigo(0);
			respuesta.setMensaje("error "+e.getMessage());
		}
		return respuesta;
	}
	
	public Respuesta nuevoPedido(Pedido pedido) {
		Respuesta respuesta = new Respuesta();
		try {
			Producto p = dao.validarStock(pedido.getNombre());
			int cantidadPedido = pedido.getCantidad();
			int stock = p.getStock();
			if(cantidadPedido > stock) {
				respuesta.setCodigo(10);
				respuesta.setMensaje("El producto existe, pero nuestro stock no tieene suficientes productos");
			} else {
				p.setStock(stock - cantidadPedido);
				dao.nuevoPedido(p);
				respuesta.setCodigo(1);
				respuesta.setMensaje("Ok tu pedido se realizo correctamente");
			}
			
		} catch (Exception e) {
			respuesta.setCodigo(0);
			respuesta.setMensaje("error de transaccion "+e.getMessage());
		}
		return respuesta;
	}
}
