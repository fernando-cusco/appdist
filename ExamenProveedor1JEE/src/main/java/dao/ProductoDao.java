package dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import modelos.Producto;

public class ProductoDao {

	@Inject
	private EntityManager em;
	
	public void nuevoProducto(Producto producto) {
		em.persist(producto);
	}
	
	public void nuevoPedido(Producto producto) {
		em.merge(producto);
	}
	
	public Producto validarStock(String nombre) {
		String jpql = "select p from Producto p where p.nombreProducto = :nombre";
		Query query = em.createQuery(jpql, Producto.class);
		query.setParameter("nombre", nombre);
		Producto p = (Producto)query.getSingleResult();
		return p;
	}
}
