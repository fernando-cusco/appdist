package dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import modelo.Usuario;

public class UsuarioDAO {

	@Inject
	private EntityManager em;

	public void nuevoUsuario(Usuario usuario) {
		em.persist(usuario);
	}

	public Usuario login(String correo, String password) {
		String jpql = "select u from Usuario u where u.correo like :correo and u.password like :password";
		Query query = em.createQuery(jpql, Usuario.class);
		query.setParameter("correo", "%"+correo+"%");
		query.setParameter("password", password);
		Usuario usuario = (Usuario) query.getSingleResult();
		return usuario;

	}
}
