package negocio;

import javax.ejb.Stateless;
import javax.inject.Inject;

import dao.UsuarioDAO;
import modelo.Usuario;
import utils.Respuesta;

@Stateless
public class UsuarioON {

	@Inject
	private UsuarioDAO usuarioDao;

	public Respuesta nuevoUsuario(Usuario usuario) {
		Respuesta respuesta = new Respuesta();
		try {
			usuarioDao.nuevoUsuario(usuario);
			respuesta.setCodigo(100);
			respuesta.setMensaje("ok");
		} catch (Exception e) {
			respuesta.setCodigo(100);
			respuesta.setMensaje("error " + e.getMessage());
		}
		return respuesta;
	}

	public Usuario login(Usuario u) {
		Usuario usuario = null;
		try {
			usuario = usuarioDao.login(u.getCorreo(), u.getPassword());
		} catch (Exception e) {
			
		}
		return usuario;
	}
}
