package services;

import java.util.Date;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import modelo.Usuario;
import negocio.UsuarioON;
import utils.Respuesta;

@Path("/usuario")
public class UsuarioService {

	@Inject
	private UsuarioON usuarioOn;

	@POST
	@Path("/nuevo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Respuesta nuevoUsuario(Usuario usuario) {
		return usuarioOn.nuevoUsuario(usuario);
	}

	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(Usuario u) {
		Usuario usuario = usuarioOn.login(u);
		if (usuario.getId() > 0) {
			long tiempo = System.currentTimeMillis();
			String key = "password";
			String jwt = Jwts.builder().signWith(SignatureAlgorithm.HS256, key)
					.setSubject(usuario.getNombres() + " " + usuario.getApellidos())
					.setIssuedAt(new Date(tiempo))
					.setIssuer("localhost")
					.setExpiration(new Date(tiempo + 9000000))
					.claim("correo", usuario.getCorreo()).compact();
			JsonObject json = Json.createObjectBuilder()
							.add("jwt", jwt).build();
			return Response.status(Response.Status.CREATED).entity(json).build();
		}
		return Response.status(Response.Status.UNAUTHORIZED).build();
	}
}
