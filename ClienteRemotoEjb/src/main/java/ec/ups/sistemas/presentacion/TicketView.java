package ec.ups.sistemas.presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Font;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import ec.ups.sistemas.negocio.en.Ticket;
import ec.ups.sistemas.negocio.en.Vehiculo;
import ec.ups.sistemas.negocio.on.GestionTicketsRemote;

import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.GridBagLayout;
import javax.swing.JTable;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class TicketView extends JFrame {

	private JPanel contentPane;
	private JTextField txtNumTicket;
	private JTextField txtHoraIngreso;
	public static JTextField txtPlaca;
	public static JTextField txtMarca;
	private JTextField txtTicketBuscar;
	private JTextField txtHoraIngresoC;
	private JTextField txtPlacaC;
	private JTextField txtTiempo;
	private JTextField txtHoraSalidaC;
	private JTextField txtMarcaC;
	private JTextField txtValor;
	private JScrollPane scrollPane;
	
	private int id;
	
	private GestionTicketsRemote operaciones;
	private JTable table;
	DefaultTableModel modelo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TicketView frame = new TicketView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TicketView() {
		modelo  = new DefaultTableModel(new Object[][] {}, new Object[] {"Numero","Fecha","Placa","Marca","Hora Ingreso"});
		this.id = 0;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 610, 413);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(tabbedPane, GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(tabbedPane, GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)
		);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Crear Tickets", null, panel, null);
		
		JLabel lblNewLabel = new JLabel("Crear Ticket");
		lblNewLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 24));
		
		JLabel lblNumeroTicket = new JLabel("Numero Ticket:");
		
		JLabel lblNewLabel_1 = new JLabel("Hora Ingreso:");
		
		JLabel lblNewLabel_2 = new JLabel("Placa:");
		
		JLabel lblNewLabel_3 = new JLabel("Marca:");
		scrollPane = new JScrollPane();
		
		txtNumTicket = new JTextField();
		txtNumTicket.setColumns(10);
		
		txtHoraIngreso = new JTextField();
		txtHoraIngreso.setColumns(10);
		
		txtPlaca = new JTextField();
		txtPlaca.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				buscarPlacaTicketIngresar();
			}
		});
		txtPlaca.setColumns(10);
		
		txtMarca = new JTextField();
		txtMarca.setColumns(10);
		
		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generarTicket();
				
			}
		});
		
		JButton btnNuevo = new JButton("Nuevo");
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(89)
							.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblNewLabel_1)
								.addComponent(lblNumeroTicket)
								.addComponent(lblNewLabel_2))
							.addGap(18)
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(txtHoraIngreso, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panel.createSequentialGroup()
									.addComponent(txtPlaca, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(lblNewLabel_3)
									.addGap(18)
									.addComponent(txtMarca, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(99))
								.addGroup(gl_panel.createSequentialGroup()
									.addComponent(txtNumTicket, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED))))
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(210)
							.addComponent(lblNewLabel))
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(181)
							.addComponent(btnRegistrar)
							.addGap(41)
							.addComponent(btnNuevo)))
					.addContainerGap(198, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(23)
					.addComponent(lblNewLabel)
					.addGap(41)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNumeroTicket)
						.addComponent(txtNumTicket, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(32)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1)
						.addComponent(txtHoraIngreso, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(41)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_2)
						.addComponent(txtPlaca, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_3)
						.addComponent(txtMarca, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnRegistrar)
						.addComponent(btnNuevo))
					.addGap(23))
		);
		panel.setLayout(gl_panel);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Cobrar Tickets", null, panel_1, null);
		
		JLabel lblNumeroTicket_1 = new JLabel("Numero Ticket:");
		
		txtTicketBuscar = new JTextField();
		txtTicketBuscar.setColumns(10);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscarTicketVehiculo(Integer.parseInt(txtTicketBuscar.getText()));
			}
		});
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Detalle Ticket", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JButton btnCobrar = new JButton("Cobrar");
		btnCobrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cobrarTicket();
			}
		});
		
		JButton btnNuevoC = new JButton("Nuevo");
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(100)
							.addComponent(lblNumeroTicket_1)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(txtTicketBuscar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnBuscar))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(68)
							.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 471, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(40, Short.MAX_VALUE))
				.addGroup(Alignment.LEADING, gl_panel_1.createSequentialGroup()
					.addGap(181)
					.addComponent(btnCobrar)
					.addPreferredGap(ComponentPlacement.RELATED, 120, Short.MAX_VALUE)
					.addComponent(btnNuevoC)
					.addGap(108))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(42)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNumeroTicket_1)
						.addComponent(txtTicketBuscar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnBuscar))
					.addGap(18)
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 196, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnCobrar)
						.addComponent(btnNuevoC))
					.addContainerGap(9, Short.MAX_VALUE))
		);
		
		JLabel lblNewLabel_4 = new JLabel("Ingreso:");
		
		JLabel lblNewLabel_5 = new JLabel("Salida:");
		
		JLabel lblNewLabel_6 = new JLabel("Placa:");
		
		JLabel lblNewLabel_7 = new JLabel("Marca:");
		
		JLabel lblNewLabel_8 = new JLabel("Tiempo:");
		
		JLabel lblNewLabel_9 = new JLabel("Valor:");
		
		txtHoraIngresoC = new JTextField();
		txtHoraIngresoC.setColumns(10);
		
		txtPlacaC = new JTextField();
		txtPlacaC.setColumns(10);
		
		txtTiempo = new JTextField();
		txtTiempo.setColumns(10);
		
		txtHoraSalidaC = new JTextField();
		txtHoraSalidaC.setColumns(10);
		
		txtMarcaC = new JTextField();
		txtMarcaC.setColumns(10);
		
		txtValor = new JTextField();
		txtValor.setColumns(10);
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(40)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel_4)
						.addComponent(lblNewLabel_6)
						.addComponent(lblNewLabel_8))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(txtHoraIngresoC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtPlacaC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtTiempo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(35)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(lblNewLabel_5)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(txtHoraSalidaC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(lblNewLabel_9)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(txtValor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(lblNewLabel_7)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(txtMarcaC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(14, Short.MAX_VALUE))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(21)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblNewLabel_5)
							.addComponent(txtHoraSalidaC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel_4)
								.addComponent(txtHoraIngresoC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(34)
							.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel_6)
								.addComponent(txtPlacaC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_7)
								.addComponent(txtMarcaC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(32)
							.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel_8)
								.addComponent(txtTiempo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_9)
								.addComponent(txtValor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(37, Short.MAX_VALUE))
		);
		panel_2.setLayout(gl_panel_2);
		panel_1.setLayout(gl_panel_1);
		
		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("Listado", null, panel_3, null);
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_3.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_panel_3.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_3.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel_3.setLayout(gbl_panel_3);
		
		JButton btnRefresar = new JButton("Refresar");
		btnRefresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				listar();
			}
		});
		GridBagConstraints gbc_btnRefresar = new GridBagConstraints();
		gbc_btnRefresar.insets = new Insets(0, 0, 5, 5);
		gbc_btnRefresar.gridx = 7;
		gbc_btnRefresar.gridy = 0;
		panel_3.add(btnRefresar, gbc_btnRefresar);
		
		table = new JTable(modelo);
		scrollPane.setViewportView(table);
		GridBagConstraints gbc_table = new GridBagConstraints();
		gbc_table.gridwidth = 9;
		gbc_table.gridheight = 3;
		gbc_table.fill = GridBagConstraints.BOTH;
		gbc_table.gridx = 0;
		gbc_table.gridy = 1;
		panel_3.add(table, gbc_table);
		contentPane.setLayout(gl_contentPane);
		
		
		
		
		
		try {
			conectar();
		} catch (Exception e) {
			e.printStackTrace();
		}
		nextTicket();
		actualizarHora();
		
		
	}
	
	public void listar() {

		List<Ticket> listado = operaciones.listarTicketsa(1);
		Object[] filas = new Object[5];
		for (int i = 0; i < listado.size(); i++) {
			filas[0] = listado.get(i).getId();
			filas[1] = listado.get(i).getFecha();
			filas[2] = listado.get(i).getHoraIngreso();
			filas[3] = listado.get(i).getUnVehiculo().getPlaca();
			filas[4] = listado.get(i).getUnVehiculo().getMarca();
			modelo.addRow(filas);
		}
		table.setModel(modelo);
		
	}
	
	public void conectar() throws Exception {
		try {  
            final Hashtable<String, Comparable> jndiProperties = new Hashtable<String, Comparable>();  
            jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");  
            jndiProperties.put("jboss.naming.client.ejb.context", true);  
            jndiProperties.put(Context.PROVIDER_URL, "http-remoting://localhost:8080");  
            jndiProperties.put(Context.SECURITY_PRINCIPAL, "ejb");  
            jndiProperties.put(Context.SECURITY_CREDENTIALS, "241996cuenca");
              
            final Context context = new InitialContext(jndiProperties);  
              
            final String lookupName = "ejb:/TicketJEE/GestionTickets!ec.ups.sistemas.negocio.on.GestionTicketsRemote";  
              
            this.operaciones = (GestionTicketsRemote) context.lookup(lookupName);  
              
        } catch (Exception ex) {  
            ex.printStackTrace();  
            throw ex;  
        }  
	}
	
	public void buscarTicketVehiculo(int id) {
		this.id = id;
		Ticket tck = operaciones.buscarNumeroTicket(id);
		if (tck.getId() == 0) {
            JOptionPane.showMessageDialog(rootPane, "El Ticket consultado no existe.");
        } else {
            if (tck.getEstado() == 0) {
                txtHoraIngresoC.setText(tck.getHoraIngreso());
                horaSalida();
                txtPlacaC.setText(tck.getUnVehiculo().getMarca());
                txtMarcaC.setText(tck.getUnVehiculo().getPlaca());
                int tiempo = operaciones.calcularTiempo(tck.getHoraIngreso(), txtHoraSalidaC.getText());
                pasarHoras(tiempo);
                double costo = operaciones.calcularValor(tiempo);
                txtValor.setText(costo + "");
            } else {
                JOptionPane.showMessageDialog(rootPane, "El Ticket consultado ya fue cancelado $$$.");
            }
        }
	}
	
	public void buscarPlacaTicketIngresar() {
		String temp = "";
        int total = txtPlaca.getText().length();
        Vehiculo v = operaciones.buscarVehiculo(txtPlaca.getText().toUpperCase());
        if (total == 7) {
            if (v.getPlaca() != null || v.getMarca() != null) {
                txtPlaca.setText(v.getPlaca().toUpperCase());
                txtMarca.setText(v.getMarca());
            } else {
                ventanaCrear();
            }
        } else if (total == 8) {
            if (v.getPlaca() != null || v.getMarca() != null) {
                txtPlaca.setText(v.getPlaca().toUpperCase());
                txtMarca.setText(v.getMarca());
            } else {
                ventanaCrear();
            }
        } else if (total > 8) {
            String plc = txtPlaca.getText().toUpperCase();

            for (int i = 0; i < plc.length() - 1; i++) {
                temp = temp.concat(String.valueOf(plc.charAt(i)));
            }
            txtPlaca.setText(temp.toUpperCase());
        }
        actualizarHora();
	}
	
	private void generarTicket() {
		if (!txtPlaca.getText().isEmpty() && !txtMarca.getText().isEmpty()) {
            Vehiculo vehiculo = operaciones.buscarIdVehiculo(txtPlaca.getText());
            Ticket t = new Ticket();
            t.setUnVehiculo(vehiculo);
            t.setFecha(new Date().toString());
            Date ahora = new Date();
            SimpleDateFormat formateador = new SimpleDateFormat("hh:mm:ss");
            t.setHoraIngreso(formateador.format(ahora));
            t.setEstado(0);
            if (!operaciones.isOpenTicket(vehiculo.getId())) {
            	operaciones.registrarTicket(t);
                txtPlaca.setText("");
                txtMarca.setText("");
                JOptionPane.showMessageDialog(rootPane, "Ticket Creado");
            } else {
                JOptionPane.showMessageDialog(rootPane, "El Vehiculo esta dentro del parqueadero.");
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "Completa los campos");
            txtPlaca.requestFocus();
        }
        limpiarRegistro();
	}
	
	private void cobrarTicket() {
		if (validarCamposPago()) {
            Ticket t = new Ticket();
            Date ahora = new Date();
            SimpleDateFormat formateador = new SimpleDateFormat("hh:mm:ss");
            t.setHoraSalida(formateador.format(ahora));
            t.setEstado(1);
            t.setTiempo(txtTiempo.getText());
            t.setValor(Float.parseFloat(txtValor.getText()));
            t.setId(id);
            boolean isPayed = operaciones.pagarTicket(t);

            if (isPayed) {
                JOptionPane.showMessageDialog(rootPane, "Pago realizado exitosamente");
            } else {
                JOptionPane.showMessageDialog(rootPane, "Ocurrio un error");
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "Busca un Ticket");
            txtTicketBuscar.requestFocus();
        }
        limpiarPago();
	}
	
	private void limpiarRegistro() {
		txtPlaca.setText("");
        txtMarca.setText("");
        actualizarHora();
        nextTicket();
	}
	private void horaSalida() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("hh:mm:ss");
        txtHoraSalidaC.setText(formateador.format(ahora));
    }
	private void pasarHoras(int tiempo) {
        long horasReales = TimeUnit.MINUTES.toHours(tiempo);
        long minutosReales = TimeUnit.MINUTES.toMinutes(tiempo) - TimeUnit.HOURS.toMinutes(TimeUnit.MINUTES.toHours(tiempo));
        txtTiempo.setText(horasReales + ":" + minutosReales);
    }
	
	private void actualizarHora() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("hh:mm:ss");
        txtHoraIngreso.setText(formateador.format(ahora));
    }
	
	private void nextTicket() {
        int nextTicket = operaciones.numeroTicketNext();
        txtNumTicket.setText(nextTicket + "");
    }
	
	private void ventanaCrear() {
		VehiculoView vv = new VehiculoView(this, true, operaciones);
        vv.txtPlaca.setText(txtPlaca.getText().toUpperCase());
        vv.setVisible(true);
	}
	
	private void limpiarPago() {
        txtHoraIngresoC.setText("");
        txtHoraSalidaC.setText("");
        txtPlacaC.setText("");
        txtMarcaC.setText("");
        txtTiempo.setText("");
        txtTicketBuscar.setText("");
        txtValor.setText("");
    }
	
	private boolean validarCamposPago() {
        boolean isValid = false;
        if (txtHoraIngresoC.getText().isEmpty() || txtHoraSalidaC.getText().isEmpty() || txtMarcaC.getText().isEmpty()
                || txtPlacaC.getText().isEmpty() || txtTiempo.getText().isEmpty() || txtValor.getText().isEmpty()) {
            isValid = false;
        } else {
            isValid = true;
        }
        return isValid;
    }
}
