package entidad;

public class Transferencia {
	private int cOrigen;
	private int cDestino;
	private double monto;
	public int getcOrigen() {
		return cOrigen;
	}
	public void setcOrigen(int cOrigen) {
		this.cOrigen = cOrigen;
	}
	public int getcDestino() {
		return cDestino;
	}
	public void setcDestino(int cDestino) {
		this.cDestino = cDestino;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	@Override
	public String toString() {
		return "Transferencia [cOrigen=" + cOrigen + ", cDestino=" + cDestino + ", monto=" + monto + "]";
	}
	
}
