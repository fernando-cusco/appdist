package entidad;

import java.util.Date;




public class Movimiento {
	
	private int id;
	private String tipoTransaccion;
	private double valor;
	
	private int cuenta;
	private Date fecha;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTipoTransaccion() {
		return tipoTransaccion;
	}
	public void setTipoTransaccion(String tipoTransaccion) {
		this.tipoTransaccion = tipoTransaccion;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public int getCuenta() {
		return cuenta;
	}
	public void setCuenta(int cuenta) {
		this.cuenta = cuenta;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	@Override
	public String toString() {
		return "Movimiento [id=" + id + ", tipoTransaccion=" + tipoTransaccion + ", valor=" + valor + ", cuenta="
				+ cuenta + ", fecha=" + fecha + "]";
	}

	
	

}
