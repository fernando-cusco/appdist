package entidad;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;






public class CuentaON {

	
	public String WS_GET_CUENTA = "http://localhost:8080/PracticaWS/rest/consulta/consultar";
	
	
	public List<Cuenta> nuevo() {
		Client client = ClientBuilder.newClient();		
		WebTarget target = client.target(WS_GET_CUENTA);
		
		
		//List<Cuenta> r = client.target(WS_GET_CUENTA).request(MediaType.APPLICATION_JSON).post(new GenericType<List<Cuenta>>(){});
		Cuenta c= new Cuenta();
		List<Cuenta> r = target.request().post(Entity.json(c), new GenericType<List<Cuenta>>() {});
		client.close();
		

		return r;
	}


}
