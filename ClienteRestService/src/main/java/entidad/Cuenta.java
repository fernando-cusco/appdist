package entidad;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Cuenta {

	private int numero;
	
	private String nombreUsuario;
	
	private Double saldo;
	
	private List<Movimiento> movimientos;
	
	public void addMovimiento(Movimiento movimiento) {
		if (movimientos== null) {
			movimientos= new ArrayList<Movimiento>();
		}
		movimientos.add(movimiento);
	}
	
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	public List<Movimiento> getMovimientos() {
		return movimientos;
	}
	public void setMovimientos(List<Movimiento> movimientos) {
		this.movimientos = movimientos;
	}
	@Override
	public String toString() {
		return "Cuenta [numero=" + numero + ", nombreUsuario=" + nombreUsuario + ", saldo=" + saldo + ", movimientos="
				+ movimientos + "]";
	}
	

}
