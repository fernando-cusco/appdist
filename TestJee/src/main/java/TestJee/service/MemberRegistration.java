/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package TestJee.service;

import TestJee.model.Member;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.logging.Logger;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless				//sin estado no guarada estado alguno en la memoria del servidor, se crea para ejectar el metodo y luego se libera
public class MemberRegistration {

    @Inject
    private Logger log;

    @Inject
    private EntityManager em;

    @Inject
    private Event<Member> memberEventSrc;

    public void register(Member member) throws Exception {
        log.info("Registering " + member.getName());
        em.persist(member);
        memberEventSrc.fire(member);
    }
}

//@Stateful //cuando el servidor se levanta los configura para almacenarce en la memoria del servidor, cuando el cliente necesita se establece una sesion se atta una instalcion de este ejb al cliente que lo solicito, para poder guardar datos, hasta que el suaurio cierra sesion 

//@Singleton la caracterisica permite guardar datos, se queda en memora del servidor, se crea una sola instancia a nivel de toda la aplicacion, se comparte con todos los usuario que los requiere, cuando necesito manejar datos a nivel del servidor para evitar cargas exesivas al db.