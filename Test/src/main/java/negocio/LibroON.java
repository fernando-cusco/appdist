package negocio;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import modelo.Libro;

@Stateless
public class LibroON {

	private List<Libro> libros = new ArrayList<>();

	public void agregarLibro(Libro libro) {
		libros.add(libro);
		System.out.println("size: " + libros.size());
	}

	public List<Libro> allLibros() {
		return this.libros;
	}

	public List<Libro> getLibros() {
		return libros;
	}

	public void setLibros(List<Libro> libros) {
		this.libros = libros;
	}

}
