package vista;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import modelo.Libro;
import negocio.LibroON;

@ManagedBean
@ApplicationScoped
public class LibroController {

	@Inject
	private LibroON on;
	
	private Libro libro;
	private List<Libro> libros;

	@PostConstruct
	public void init() {
		libro = new Libro();
		if(libros == null) {
			libros = new ArrayList<Libro>();
		}
	}

	public void agregarLibro() {
		
		libros.add(libro);
	}

	public Libro getLibro() {
		return libro;
	}

	public void setLibro(Libro libro) {
		this.libro = libro;
	}

	public List<Libro> getLibros() {
		return libros;
	}

	public void setLibros(List<Libro> libros) {
		this.libros = libros;
	}

}
