package negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import dao.ProductoDao;
import modelos.Producto;
import utils.Respuesta;

@Stateless
public class ProductoON {

	@Inject
	private ProductoDao dao;
	
	public Respuesta recibirPedido(Producto producto) {
		Respuesta respuesta = new Respuesta();
		try {
			int cantidad = producto.getStock();
			Producto p = dao.buscar(producto.getId());
			int stock = p.getStock();
			p.setStock(cantidad+stock);
			dao.actualizarStock(p);
			respuesta.setCodigo(1);
			respuesta.setMensaje("Stock actualizado, ahora tiene "+(cantidad+stock)+" "+p.getNombreProducto());
		} catch (Exception e) {
			respuesta.setCodigo(0);
			respuesta.setMensaje("Error "+e.getMessage());
		}
		return respuesta;
	}
	
	public List<Producto> misProductos() {
		return dao.misProductos();
	}
	
	public void nuevoProducto(Producto pro) {
		dao.nuevoProducto(pro);
	}
	
	
	
}
