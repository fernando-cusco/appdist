package negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import dao.ProveedorDao;
import modelos.Proveedor;

@Stateless
public class ProveedorON {

	@Inject
	private ProveedorDao dao;
	
	public List<Proveedor> proveedores() {
		return dao.proveedores();
	}
	
	public Proveedor buscar(int id) {
		return dao.buscar(id);
	}
		
}
