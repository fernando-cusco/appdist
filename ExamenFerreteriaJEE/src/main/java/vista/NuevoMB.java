package vista;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import modelos.Producto;
import modelos.Proveedor;
import negocio.ProductoON;
import negocio.ProveedorON;

@ManagedBean
public class NuevoMB {
	
	@Inject
	private ProveedorON pro;
	
	@Inject
	private ProductoON prd;
	
	private Producto producto;

	private List<Proveedor> proveedores;
	
	private int id;
	
	@PostConstruct
	public void init() {
		producto = new Producto();
		proveedores = pro.proveedores();
	}
	
	
	public String nuevo() {
		Proveedor p = pro.buscar(id);
		Proveedor t = new Proveedor();
		p.setId(p.getId());
		p.setNombre(p.getNombre());
		producto.setProveedor(t);
		t.agregar(producto);
		prd.nuevoProducto(producto);
		return null;
	}
	
	
	
	public List<Proveedor> getProveedores() {
		return proveedores;
	}



	public void setProveedores(List<Proveedor> proveedores) {
		this.proveedores = proveedores;
	}



	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}
	
	
	
}
