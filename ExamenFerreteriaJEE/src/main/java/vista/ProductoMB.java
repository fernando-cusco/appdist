package vista;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import modelos.Producto;
import negocio.ProductoON;
import utils.Pedido;
import utils.Respuesta;

@ManagedBean
public class ProductoMB {

	@Inject
	private ProductoON on;

	private Producto producto;

	private List<Producto> productos;

	
	private int cantidad;

	private String mensajeServidor;
	private String mensajeLocal;

	@PostConstruct
	public void init() {
		producto = new Producto();
		productos = new ArrayList<Producto>();
		mensajeServidor = "";
		mensajeLocal = "";
		cantidad = 0;
		listar();

	}

	public void listar() {
		productos = on.misProductos();
	}

	public String nuevoPedido(Producto pro) {
		System.out.println("PROVEEDOR " + pro.getProveedor().getNombre());
		String url = "";
		if (pro.getProveedor().getNombre().equals("megahierro")) {
			url = "http://localhost:8080/ExamenProveedor1JEE/api/pedidos/pedido";
		}
		if (pro.getProveedor().getNombre().equals("kiwi")) {
			url = "http://localhost:8080/ExamenProveedor2JEE/api/pedidos/pedido";
		}

		Client cl = ClientBuilder.newClient();
		WebTarget tar = cl.target(url);
		Pedido p = new Pedido();
		p.setCantidad(cantidad);
		p.setNombre(pro.getNombreProducto());
		Response res = tar.request().post(Entity.json(p));
		Respuesta r = res.readEntity(Respuesta.class);
		System.out.println("MENSAJE DEL SERVIDOR: " + r.toString());
		mensajeServidor = r.toString();
		if (r.getCodigo() == 1) {
			producto.setId(pro.getId());
			producto.setNombreProducto(pro.getNombreProducto());
			producto.setStock(cantidad);
			Respuesta rs = on.recibirPedido(producto);
			System.out.println("MENSAJE LOCAL " + rs.toString());
			mensajeLocal = rs.toString();
			listar();
		}
		cl.close();
		return null;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public List<Producto> getProductos() {
		return productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}



	public String getMensajeServidor() {
		return mensajeServidor;
	}

	public void setMensajeServidor(String mensajeServidor) {
		this.mensajeServidor = mensajeServidor;
	}

	public String getMensajeLocal() {
		return mensajeLocal;
	}

	public void setMensajeLocal(String mensajeLocal) {
		this.mensajeLocal = mensajeLocal;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	
}
