package dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import modelos.Producto;

public class ProductoDao {

	@Inject
	private EntityManager em;
	
	public void actualizarStock(Producto producto) {
		em.merge(producto);
	}
	
	public Producto buscar(int id) {
		return em.find(Producto.class, id);
	}
	
	public List<Producto> misProductos() {
		System.out.println("llegando");
		String jpql = "select p from Producto p JOIN FETCH p.proveedor";
		Query query = em.createQuery(jpql, Producto.class);
		List<Producto> productos = query.getResultList();
		return productos;
	}
	
	public void nuevoProducto(Producto p) {
		em.persist(p);
	}
	
	
}
