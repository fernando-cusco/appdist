package dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import modelos.Proveedor;

public class ProveedorDao {

	
	@Inject
	private EntityManager em;
	
	public List<Proveedor> proveedores() {
		String jpql = "select p from Proveedor p JOIN FETCH p.productos";
		Query query = em.createQuery(jpql, Proveedor.class);
		List<Proveedor> p = query.getResultList();
		return p;
	}
	
	public Proveedor buscar(int id) {
		return em.find(Proveedor.class, id);
	}
}
