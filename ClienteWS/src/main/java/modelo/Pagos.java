package modelo;

import java.io.Serializable;
import java.util.Date;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


public class Pagos implements Serializable{

	private int id;
	private int numeroPago;
	private Date FechaPago;
	private Double valor;
	private int cuenta;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumeroPago() {
		return numeroPago;
	}
	public void setNumeroPago(int numeroPago) {
		this.numeroPago = numeroPago;
	}
	public Date getFechaPago() {
		return FechaPago;
	}
	public void setFechaPago(Date fechaPago) {
		FechaPago = fechaPago;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public int getCuenta() {
		return cuenta;
	}
	public void setCuenta(int cuenta) {
		this.cuenta = cuenta;
	}
	@Override
	public String toString() {
		return "Pagos [id=" + id + ", numeroPago=" + numeroPago + ", FechaPago=" + FechaPago + ", valor=" + valor
				+ ", cuenta=" + cuenta + "]";
	}
	
	
}
