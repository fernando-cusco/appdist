package modelo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Cuenta implements Serializable{

	private int id;
	private String nombreUsuario;
	
	private List<Credito> creditos;
	
	public void addMovimiento(Credito credito) {
		if (creditos== null) {
			creditos= new ArrayList<Credito>();
		}
		creditos.add(credito);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public List<Credito> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<Credito> creditos) {
		this.creditos = creditos;
	}

	@Override
	public String toString() {
		return "Cuenta [id=" + id + ", nombreUsuario=" + nombreUsuario + ", creditos=" + creditos + "]";
	}
	
	

}
