package modelo;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import modelo.Credito;
import modelo.Cuenta;
import modelo.Pagos;
@ManagedBean
public class CreditoON {

	/*private Credito credito;

	@PostConstruct
	public void init() {
		this.credito= new Credito();
	}*/
	
	public String pathInsert="http://localhost:8080/PruebaWS/rest/credito/insertar";
	public String pathConsulta ="http://localhost:8080/PruebaWS/rest/credito/consultar";
	
	public void nuevoCredito(Credito credito) {
		Client cl = ClientBuilder.newClient();
		WebTarget tar = cl.target(pathInsert);
				
		Response res = tar.request().post(Entity.json(credito));
		System.out.println(credito.toString());
		System.out.println("RESPUESTA DEL SERVIDOR: "+res.toString());
		cl.close();
			
	 }
	public Credito credito(int cuenta) {
		Client client = ClientBuilder.newClient();		
		WebTarget target = client.target(pathConsulta).queryParam("cuenta", cuenta);
		
		Credito credito=target.request().get(new GenericType<Credito>() {
		
		});
		client.close();
		return credito;
	}
	

}
