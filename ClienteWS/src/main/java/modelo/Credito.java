package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Credito implements Serializable {
	
	private int id;
	private double monto;
	private int mes;
	private Date fechappago;
	private int cuenta;
	private List<Pagos> pagos;
	
	public void addPagos(Pagos pago) {
		if (pagos== null) {
			pagos= new ArrayList<Pagos>();
		}
		pagos.add(pago);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public Date getFechappago() {
		return fechappago;
	}

	public void setFechappago(Date fechappago) {
		this.fechappago = fechappago;
	}

	public List<Pagos> getPagos() {
		return pagos;
	}

	public void setPagos(List<Pagos> pagos) {
		this.pagos = pagos;
	}

	public int getCuenta() {
		return cuenta;
	}

	public void setCuenta(int cuenta) {
		this.cuenta = cuenta;
	}

	@Override
	public String toString() {
		return "Credito [id=" + id + ", monto=" + monto + ", mes=" + mes + ", fechappago=" + fechappago + ", cuenta="
				+ cuenta + ", pagos=" + pagos + "]";
	}

	}
