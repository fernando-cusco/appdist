package webservices;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import modelo.Credito;
import modelo.Cuenta;
import negocio.CreditoON;
import negocio.CuentaOn;

@WebService
public class SoapService {

	@Inject
	private CreditoON on;
	
	@Inject
	private CuentaOn onc;
	
	
	@WebMethod
	public void crearCuenta(Cuenta cuenta) {
		onc.crearCuenta(cuenta);
	}
	
	@WebMethod
	public Respuesta nuevoCredito(Solicitud solicitud) {
		Respuesta respuesta = on.nuevoCredito(solicitud);
		return respuesta;
	}
	
	
	
	@WebMethod
	public Credito pagos(int folio) {
		return on.pagos(folio);
	}
	
	@WebMethod
	public Cuenta misCreditos(int numero) {
		return on.misCreditos(numero);
	}
}
