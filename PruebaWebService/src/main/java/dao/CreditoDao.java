package dao;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import modelo.Credito;
import modelo.Cuenta;
import modelo.Pago;

public class CreditoDao {

	@Inject
	private EntityManager em;
	
	public void crearCredito(Credito credito) {
		em.persist(credito);
	}
	
	
	public Credito pagos(int folio) {
		String jqpl = "SELECT c FROM Credito c JOIN FETCH c.pagos where c.folio = :folio";
		Query query = em.createQuery(jqpl, Credito.class);
		query.setParameter("folio", folio);
		Credito cli = (Credito) query.getSingleResult();
		List<Pago> pagos = new ArrayList<>();
		for (Pago pago : cli.getPagos()) {
			pagos.add(pago);
		}
		return cli;
	}
}
