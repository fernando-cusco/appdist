package dao;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;


import modelo.Credito;
import modelo.Cuenta;
import modelo.Pago;


public class CuentaDao {

	@Inject
	private EntityManager em;
	
	public void crearCuenta(Cuenta cuenta) {
		em.persist(cuenta);
	}
	
	public Cuenta buscar(int numero) {
		String jpql = "SELECT c FROM Cuenta c where c.numero = :numero";
		Query query = em.createQuery(jpql, Cuenta.class);
		query.setParameter("numero", numero);
		Cuenta cuenta = (Cuenta) query.getSingleResult();
		return cuenta;
	}
	
	public Cuenta misCreditos(int numero) {
		String jqpl = "SELECT c FROM Cuenta c JOIN FETCH c.creditos where c.numero = :numero";
		Query query = em.createQuery(jqpl, Cuenta.class);
		query.setParameter("numero", numero);
		Cuenta cli = (Cuenta) query.getSingleResult();
		List<Credito> creditos = new ArrayList<>();
		for (Credito credito : cli.getCreditos()) {
			creditos.add(credito);
			for (Pago pago : credito.getPagos()) {
				pago.getId();
			}
		}
		return cli;
	}

}

