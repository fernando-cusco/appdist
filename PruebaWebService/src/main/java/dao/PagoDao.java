package dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;


import modelo.Pago;

public class PagoDao {
	@Inject
	private EntityManager em;
	
	public void nuevoPago(Pago pago) {
		em.persist(pago);
	}
}
