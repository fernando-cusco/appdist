package negocio;

import javax.ejb.Stateless;
import javax.inject.Inject;

import dao.CuentaDao;
import modelo.Cuenta;

@Stateless
public class CuentaOn {

	@Inject
	private CuentaDao dao;
	
	public void crearCuenta(Cuenta cuenta) {
		dao.crearCuenta(cuenta);
	}
}
