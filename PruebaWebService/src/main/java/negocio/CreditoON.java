package negocio;

import javax.ejb.Stateless;
import javax.inject.Inject;

import dao.CreditoDao;
import dao.CuentaDao;
import modelo.Credito;
import modelo.Cuenta;
import modelo.Pago;
import webservices.Respuesta;
import webservices.Solicitud;

@Stateless
public class CreditoON {

	@Inject
	private CreditoDao dao;
	
	@Inject
	private CuentaDao daoc;
	
	
	public Respuesta nuevoCredito(Solicitud solicitud) {
		Double cuota = 0.0;
		Respuesta r = new Respuesta();
		Cuenta cuenta = null;
		try {
			cuenta = daoc.buscar(solicitud.getNumero());
		} catch (Exception e) {
			r.setCodigo(0);
			r.setMensaje("Error "+e.getMessage());
		}
		if(cuenta == null) {
			r.setCodigo(0);
			r.setMensaje("Cuenta No existe");
		} else {
			Credito credito = new Credito();
			int folio = 0;
			credito.setMonto(solicitud.getMonto());
			for(int i = 1; i<=5; i++) {
				folio = (int)(Math.random()*6 + 1);
			}
			credito.setFolio(folio);
			//cuota mensual
			cuota = solicitud.getMonto() / solicitud.getMeses();
			
			//numero de pagos
			int dia = 20;
			int mes = 1;
			for (int i = 0; i < solicitud.getMeses(); i++) {
				Pago p = new Pago();
				p.setFecha(dia+"/"+mes+"/2020");
				p.setPago(cuota);
				credito.nuevoPago(p);
				mes++;
			}
			cuenta.nuevoCredito(credito);
			dao.crearCredito(credito);
			r.setCodigo(1);
			r.setMensaje("Ok tu credito esta aprobado, tu numero de credito es: "+folio);
		}
		return r;
	}
	
	public Cuenta misCreditos(int numero) {
		return daoc.misCreditos(numero);
	}
	
	public Credito pagos(int folio) {
		return dao.pagos(folio);
	}
}
