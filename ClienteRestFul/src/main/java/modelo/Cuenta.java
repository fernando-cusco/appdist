package modelo;

import java.util.ArrayList;
import java.util.List;



public class Cuenta {

	
	private String nombreUsuario;
	
	private int numero;
	
	
	private List<Movimiento> movimientos;
	private double saldo;
	
	public void agregarMovimiento(Movimiento m) {
		if(movimientos == null) {
			movimientos = new ArrayList<Movimiento>();
		}
		movimientos.add(m);
	}


	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public List<Movimiento> getMovimientos() {
		return movimientos;
	}

	public void setMovimientos(List<Movimiento> movimientos) {
		this.movimientos = movimientos;
	}
	
	

	public double getSaldo() {
		return saldo;
	}


	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}


	@Override
	public String toString() {
		return "Cuenta [nombreUsuario=" + nombreUsuario + "\n numero=" + numero + "\n movimientos=" + movimientos
				+ "\n saldo=" + saldo + "]";
	}


	
	
	
}
