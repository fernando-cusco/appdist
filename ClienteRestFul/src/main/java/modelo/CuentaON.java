package modelo;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;






public class CuentaON {

	
	//public String WS_GET_CUENTA = "http://localhost:8080/PracticaRest-Soap/api/movimiento/nuevo";
	public String WS_GET_CUENTA2 ="http://localhost:8080/PracticaWS/rest/consulta/consultar2";
	
	public List<Cuenta> nuevo(int cuenta) {
		Client client = ClientBuilder.newClient();		
		WebTarget target = client.target(WS_GET_CUENTA2).queryParam("cuenta", cuenta);
		
		List<Cuenta> movimientos=target.request().get(new GenericType<List<Cuenta>>() {
		
		});
		client.close();
		return movimientos;
	}


}
