package ec.edu.ups.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Path("/operaciones")
public class OperacionesService {

	@GET
	@Path("/suma")
	@Produces("application/json")
	public int sumar(@QueryParam("xx") int a, @QueryParam("yy") int b) {
		return a + b;
	}

	@POST
	@Path("/accion")
	@Produces("application/json")
	@Consumes("application/json")
	public Respuesta accionX(Object obj) {

		System.out.println();
		Respuesta r = new Respuesta();
		r.setCodigo(99);
		r.setMensaje("OK");
		return r;
	}
}
