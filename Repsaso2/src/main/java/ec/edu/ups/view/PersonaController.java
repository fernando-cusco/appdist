package ec.edu.ups.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import ec.edu.ups.modelo.Persona;
import ec.edu.ups.modelo.PersonaTelefono;
import ec.edu.ups.modelo.Telefono;
import ec.edu.ups.negocio.PersonaON;

@ManagedBean
@ViewScoped
public class PersonaController {

	private int edad;

	private Persona persona = new Persona();

	private List<Persona> listadoPersonas;

	private int id;

	private String filtro;

	@Inject
	private PersonaON pON;

	@PostConstruct
	public void init() {
		persona = new Persona();
		persona.addTelefono(new Telefono());
		listadoPersonas = pON.getListadoPersonas();
	}

	public void loadData() {
		System.out.println("codigo editar " + id);
		if (id == 0)
			return;
		persona = pON.getPersona(id);
		System.out.println(persona.getCodigo() + " " + persona.getNombre());
		System.out.println("#telefonos: " + " " + persona.getTelefonos().size());
		for (Telefono t : persona.getTelefonos()) {
			System.out.println("\t" + t);
		}

	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public List<Persona> getListadoPersonas() {
		return listadoPersonas;
	}

	public void setListadoPersonas(List<Persona> listadoPersonas) {
		this.listadoPersonas = listadoPersonas;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String cargarDatos() {

		try {
			pON.guardar(persona);
			init();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public String editar(int codigo) {

		return "personas?faces-redirect=true&id=" + codigo;
	}

	public String borrar(int codigo) {
		System.out.println("codigo borrar " + codigo);

		try {
			pON.borrar(codigo);
			init();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error " + e.getMessage());
			e.printStackTrace();
		}

		return null;
	}

	public void addTelefono() {
		persona.addTelefono(new Telefono());
		System.out.println("cnt " + persona.getTelefonos().size());
	}

	public String nuevo() {
		persona = new Persona();
		return "personas";
	}

	public String listado() {
		return "listado-personas";
	}

	public String listapersonacontelefono() {
		System.out.println("1");
		List<PersonaTelefono> personaTelefonos = pON.listapersonacontelefono(filtro);
		return null;
	}

	public String getFiltro() {
		return filtro;
	}

	public void setFiltro(String filtro) {
		this.filtro = filtro;
	}

}
