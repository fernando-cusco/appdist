package ec.edu.ups.negocio;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import ec.edu.ups.dao.TipoTelefonoDAO;
import ec.edu.ups.modelo.TipoTelefono;

@Startup
@Singleton
public class Instalacion {

	@Inject
	private TipoTelefonoDAO dao;

	private List<TipoTelefono> telefonos;

	@PostConstruct
	public void init() {

		System.out.println("Se inicializo");

		telefonos = dao.getTiposTelefono();
		if (telefonos.size() == 0) {
			TipoTelefono tp = new TipoTelefono();
			tp.setNombre("Convencional");
			dao.insert(tp);

			TipoTelefono tp1 = new TipoTelefono();
			tp1.setNombre("Celular");
			dao.insert(tp1);

			telefonos = dao.getTiposTelefono();

		}
	}

	public List<TipoTelefono> getTelefonos() {
		return telefonos;
	}

}
