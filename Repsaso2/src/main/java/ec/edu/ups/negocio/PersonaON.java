package ec.edu.ups.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.dao.PersonaDAO;
import ec.edu.ups.modelo.Persona;
import ec.edu.ups.modelo.PersonaTelefono;
import ec.edu.ups.modelo.Telefono;
import ec.edu.ups.modelo.TipoTelefono;

@Stateless
public class PersonaON {

	@Inject
	private Instalacion init;

	@Inject
	private PersonaDAO dao;

	public void guardar(Persona p) throws Exception {

		if (p.getNombre().length() < 5)
			throw new Exception("Dimension corta");

		List<TipoTelefono> tipos = init.getTelefonos();

		for (Telefono tel : p.getTelefonos()) {
			for (TipoTelefono tt : tipos) {
				if (tel.getIdTipo() == tt.getCodigo())
					tel.setTipo(tt);
			}
		}

		dao.save(p);
	}

	public List<Persona> getListadoPersonas() {
		return dao.getPersonas();
	}

	public void borrar(int codigo) throws Exception {
		try {
			dao.delete(codigo);
		} catch (Exception e) {
			throw new Exception("Error al borrar " + e.getMessage());
		}

	}

	public Persona getPersona(int codigo) {
		Persona aux = dao.read3(codigo);

		return aux;

	}

	public List<PersonaTelefono> listapersonacontelefono(String text) {
		System.out.println("2");
		return dao.listapersonacontelefono(text);
	}
}
