package ec.edu.ups.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import ec.edu.ups.modelo.TipoTelefono;

@Stateless
public class TipoTelefonoDAO {

	@Inject
	private EntityManager em;

	public void insert(TipoTelefono tipo) {
		em.persist(tipo);
	}

	public List<TipoTelefono> getTiposTelefono() {
		String jpql = "SELECT p FROM TipoTelefono p ";

		Query q = em.createQuery(jpql, TipoTelefono.class);

		List<TipoTelefono> tipos = q.getResultList();
		return tipos;
	}
}
