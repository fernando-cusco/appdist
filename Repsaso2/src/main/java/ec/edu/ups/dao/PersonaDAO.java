package ec.edu.ups.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import ec.edu.ups.modelo.Persona;
import ec.edu.ups.modelo.PersonaTelefono;
import ec.edu.ups.modelo.Telefono;

@Stateless
public class PersonaDAO {

	@Inject
	private EntityManager em;

	public void save(Persona p) {
		if (this.read(p.getCodigo()) != null)
			this.update(p);
		else
			this.create(p);

	}

	public void create(Persona p) {
		em.persist(p);
	}

	public Persona read(int id) {
		return em.find(Persona.class, id);
	}

	/**
	 * Opcion 1 de consulta por demanda para relacion LAZY
	 */
	public Persona read2(int id) {
		Persona p = em.find(Persona.class, id);
		p.getTelefonos().size();

		return p;
	}

	public Persona read3(int id) {
		/*
		 * String jpql = "SELECT p " + "	 FROM Persona p " +
		 * "		  JOIN FETCH p.telefonos t " +
		 * "		  JOIN FETCH p.direcciones d 	" + " WHERE p.codigo = ?";
		 */

		String jpql = "SELECT p " + "	 FROM Persona p " + "		  JOIN FETCH p.telefonos t "
				+ " WHERE p.codigo = :a";

		Query q = em.createQuery(jpql, Persona.class);
		q.setParameter("a", id);
		Persona p = (Persona) q.getSingleResult();

		return p;
	}

	public void update(Persona p) {

		em.merge(p);
	}

	public void delete(int id) {
		Persona p = read(id);
		em.remove(p);
	}

	public List<Persona> getPersonas() {
		String jpql = "SELECT p FROM Persona p ";

		Query q = em.createQuery(jpql, Persona.class);

		List<Persona> personas = q.getResultList();
		return personas;
	}

	public List<Persona> getPersonasPorNombre(String filtroBusqued) {
		String jpql = "SELECT p FROM Persona p " + "	WHERE p.nombre LIKE :filtro ";

		Query q = em.createQuery(jpql, Persona.class);
		q.setParameter("filtro", "%" + filtroBusqued + "%");

		List<Persona> personas = q.getResultList();
		return personas;
	}

	public List<PersonaTelefono> listapersonacontelefono(String text) {
		System.out.println("3" + text);
//		 String jpql = "SELECT p "
//				+ " FROM Persona p "
//				+ " JOIN FETCH p.telefonos t "
//				+ " WHERE UPPER(p.nombre) LIKE UPPER(:filtro)";

		String jpql = "SELECT t " + " FROM Telefono t " + " JOIN FETCH t.persona p "
				+ " WHERE UPPER(p.nombre) LIKE UPPER(:filtro)" + " OR t.numero LIKE :filtro";

		Query q = em.createQuery(jpql, Telefono.class);
		q.setParameter("filtro", "%" + text.trim() + "%");

		List<Telefono> telefonos = q.getResultList();
		List<PersonaTelefono> personaTelefonos = new ArrayList<PersonaTelefono>();
		for (Telefono t : telefonos) {
			Persona p = t.getPersona();
			PersonaTelefono personaTelefono = new PersonaTelefono();
			personaTelefono.setCodigoper(p.getCodigo());
			personaTelefono.setNombre(p.getNombre());
			List<Telefono> telefs = p.getTelefonos();
			for (Telefono tel : telefs) {
				System.out.println(tel.getNumero());
				personaTelefono.setCodigotel(tel.getCodigo());
				personaTelefono.setNumero(tel.getNumero());
				personaTelefonos.add(personaTelefono);
			}
		}

		return personaTelefonos;

	}
}
