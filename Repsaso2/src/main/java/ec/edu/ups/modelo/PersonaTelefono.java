package ec.edu.ups.modelo;

public class PersonaTelefono {

	private int codigoper;
	private int codigotel;
	private String nombre;
	private String numero;

	public int getCodigoper() {
		return codigoper;
	}

	public void setCodigoper(int codigoper) {
		this.codigoper = codigoper;
	}

	public int getCodigotel() {
		return codigotel;
	}

	public void setCodigotel(int codigotel) {
		this.codigotel = codigotel;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

}
