package main;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;

import negocio.Remoto;

public class Inicio {
	
	private Remoto operaciones;

	
	public void conectar() throws Exception {
		try {  
            final Hashtable<String, Comparable> jndiProperties =  
                    new Hashtable<String, Comparable>();  
            jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY,  
                    "org.wildfly.naming.client.WildFlyInitialContextFactory");  
            jndiProperties.put("jboss.naming.client.ejb.context", true);  
              
            jndiProperties.put(Context.PROVIDER_URL, "http-remoting://localhost:8080");  
            jndiProperties.put(Context.SECURITY_PRINCIPAL, "ejb");  
            jndiProperties.put(Context.SECURITY_CREDENTIALS, "241996cuenca");  
              
            final Context context = new InitialContext(jndiProperties);  
              
            final String lookupName = "ejb:/evaluacionfernandocuscoservidor/ServicioMensaje!negocio.Remoto";  
              
            this.operaciones = (Remoto) context.lookup(lookupName);  
              
        } catch (Exception ex) {  
            ex.printStackTrace();  
            throw ex;  
        }  
	}
	
	public void listar() {
		System.out.println("LISTANDO EN EL CLIENTE EJB DE ESCRITORIO");
		for (int i = 0; i < operaciones.verMensajes().size(); i++) {
			System.out.println(operaciones.verMensajes().get(i).getContenido() +" "+operaciones.verMensajes().get(i).getNumeros());
			
		}
	}
	public static void main(String[] args) {
		Inicio i = new Inicio();
		try {
			i.conectar();
			i.listar();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

}
//http://localhost:8080/evaluacionfernandocuscoservidor/faces/enviar.xhtml
