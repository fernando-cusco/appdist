package modelo;

import java.io.Serializable;
import java.util.List;


public class Mensaje implements Serializable{
	
	private int id;
	private String contenido;
	private String numeros;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	public String getNumeros() {
		return numeros;
	}
	public void setNumeros(String numeros) {
		this.numeros = numeros;
	}
	
}
