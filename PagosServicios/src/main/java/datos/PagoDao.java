package datos;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import modelo.Cliente;

public class PagoDao {

	@Inject
	private EntityManager em;
	
	public Cliente buscar(String cedula) {
		String jpql = "Select c From Cliente c join fetch c.pagos p where c.cedula = :cedula and p.total > 0";
		Query query = em.createQuery(jpql, Cliente.class);
		query.setParameter("cedula",cedula);
		Cliente cliente = (Cliente) query.getSingleResult();
		return cliente;
	}
}
