package negocio;

import javax.ejb.Stateless;
import javax.inject.Inject;

import datos.PagoDao;
import modelo.Cliente;

@Stateless
public class ClienteON {

	@Inject
	private PagoDao dao;
	
	
	public Cliente buscar(String cedula) {
		return dao.buscar(cedula);
	}
}
