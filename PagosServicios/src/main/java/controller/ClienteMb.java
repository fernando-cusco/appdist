package controller;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import modelo.Cliente;
import negocio.ClienteON;

@ManagedBean
public class ClienteMb {

	@Inject
	private ClienteON on;
	
	private Cliente cliente;
	
	private String cedula;
	
	@PostConstruct
	public void init() {
		cliente = new Cliente();
	}
	
	public String buscar() {
		cliente = on.buscar(cedula);
		System.out.println(cliente.toString());
		return "";
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	
	
}
