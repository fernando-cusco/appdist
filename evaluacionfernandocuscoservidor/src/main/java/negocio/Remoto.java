package negocio;

import java.util.List;

import javax.ejb.Remote;

import modelo.Mensaje;

@Remote
public interface Remoto {
public void enviarMensaje(Mensaje mensaje);
	
	public List<Mensaje> verMensajes();
}
