package negocio;

import java.util.List;

import javax.ejb.Stateless;

import datos.MensajeDao;
import modelo.Mensaje;

@Stateless
public class ServicioMensaje implements Remoto, ServicioLocal{

	private MensajeDao mensajedao = new MensajeDao();
	@Override
	public void enviarMensaje(Mensaje mensaje) {
		System.out.println("MENSAJE QUE ESTOY ENVIANDO EN EL SERVIDOR");
		Mensaje mt = new Mensaje();
		mt.setContenido(mensaje.getContenido());
		String numeros[] = mensaje.getNumeros().split(",");
		for(int i = 0; i < numeros.length; i++ ) {
			System.out.println(mt.getContenido()+" "+numeros[i]);
			mt.setNumeros(numeros[i]);
			mensajedao.insertMensaje(mt);
		}
		
	}

	@Override
	public List<Mensaje> verMensajes() {
		List<Mensaje> m = mensajedao.mostrarMensajes();
		System.out.println("LISTANDO MENSAJE EN EL SERVIDOR");
		for (int i = 0; i < m.size(); i++) {
			System.out.println(m.get(i).getContenido()+" "+m.get(i).getNumeros());
		}
		return m;
	}

}
