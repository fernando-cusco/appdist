package negocio;

import java.util.List;

import javax.ejb.Local;

import modelo.Mensaje;

@Local
public interface ServicioLocal {
	
	public void enviarMensaje(Mensaje mensaje);
	
	public List<Mensaje> verMensajes();

}
