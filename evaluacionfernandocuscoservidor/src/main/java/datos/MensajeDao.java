package datos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modelo.Mensaje;


public class MensajeDao {
	
	private Conexion con;
	
	public boolean insertMensaje(Mensaje mensaje) {
		boolean flag = false;
		con = new Conexion();
		PreparedStatement statement = null;
		try {
			String sql = "insert into Mensajes(contenido, numero) values(?, ?);";
			statement = con.conexion().prepareStatement(sql);
			statement.setString(1, mensaje.getContenido());
			statement.setString(2, mensaje.getNumeros());
			flag = statement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
			con.cerrarConexion();
		}
		return flag;
	}
	
	public List<Mensaje> mostrarMensajes() {
		List<Mensaje> sms = new ArrayList<Mensaje>();
		con = new Conexion();
        Statement statement = null;
        ResultSet result = null;
        try {
			String sql = "select contenido, numero from mensajes;";
			statement = con.conexion().createStatement();
            result = statement.executeQuery(sql);
            while(result.next()) {
            	Mensaje m = new Mensaje();
            	m.setContenido(result.getString(1));
            	m.setNumeros(result.getString(2));
            	sms.add(m);
            }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				result.close();
				statement.close();
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
			con.cerrarConexion();
		}
        return sms;
	}

}
