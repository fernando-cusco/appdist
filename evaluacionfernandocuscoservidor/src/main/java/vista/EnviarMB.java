package vista;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import modelo.Mensaje;
import negocio.ServicioLocal;

@ManagedBean
public class EnviarMB {
	
	@Inject
	private ServicioLocal servicio;
	
	
	
	private Mensaje sms = new Mensaje();
	
	
	
	public Mensaje getSms() {
		return sms;
	}
	public void setSms(Mensaje sms) {
		this.sms = sms;
	}
	
	
	public String enviarMensaje() {
		servicio.enviarMensaje(sms);
		return null;
	}
	
	
	

}
