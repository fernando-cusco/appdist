
package ec.edu.ups;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ec.edu.ups package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CrearCuenta_QNAME = new QName("http://webservices/", "crearCuenta");
    private final static QName _Pagos_QNAME = new QName("http://webservices/", "pagos");
    private final static QName _NuevoCreditoResponse_QNAME = new QName("http://webservices/", "nuevoCreditoResponse");
    private final static QName _MisCreditos_QNAME = new QName("http://webservices/", "misCreditos");
    private final static QName _CrearCuentaResponse_QNAME = new QName("http://webservices/", "crearCuentaResponse");
    private final static QName _PagosResponse_QNAME = new QName("http://webservices/", "pagosResponse");
    private final static QName _NuevoCredito_QNAME = new QName("http://webservices/", "nuevoCredito");
    private final static QName _MisCreditosResponse_QNAME = new QName("http://webservices/", "misCreditosResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ec.edu.ups
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MisCreditosResponse }
     * 
     */
    public MisCreditosResponse createMisCreditosResponse() {
        return new MisCreditosResponse();
    }

    /**
     * Create an instance of {@link NuevoCredito }
     * 
     */
    public NuevoCredito createNuevoCredito() {
        return new NuevoCredito();
    }

    /**
     * Create an instance of {@link PagosResponse }
     * 
     */
    public PagosResponse createPagosResponse() {
        return new PagosResponse();
    }

    /**
     * Create an instance of {@link CrearCuentaResponse }
     * 
     */
    public CrearCuentaResponse createCrearCuentaResponse() {
        return new CrearCuentaResponse();
    }

    /**
     * Create an instance of {@link MisCreditos }
     * 
     */
    public MisCreditos createMisCreditos() {
        return new MisCreditos();
    }

    /**
     * Create an instance of {@link NuevoCreditoResponse }
     * 
     */
    public NuevoCreditoResponse createNuevoCreditoResponse() {
        return new NuevoCreditoResponse();
    }

    /**
     * Create an instance of {@link CrearCuenta }
     * 
     */
    public CrearCuenta createCrearCuenta() {
        return new CrearCuenta();
    }

    /**
     * Create an instance of {@link Pagos }
     * 
     */
    public Pagos createPagos() {
        return new Pagos();
    }

    /**
     * Create an instance of {@link Pago }
     * 
     */
    public Pago createPago() {
        return new Pago();
    }

    /**
     * Create an instance of {@link Solicitud }
     * 
     */
    public Solicitud createSolicitud() {
        return new Solicitud();
    }

    /**
     * Create an instance of {@link Cuenta }
     * 
     */
    public Cuenta createCuenta() {
        return new Cuenta();
    }

    /**
     * Create an instance of {@link Credito }
     * 
     */
    public Credito createCredito() {
        return new Credito();
    }

    /**
     * Create an instance of {@link Respuesta }
     * 
     */
    public Respuesta createRespuesta() {
        return new Respuesta();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearCuenta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "crearCuenta")
    public JAXBElement<CrearCuenta> createCrearCuenta(CrearCuenta value) {
        return new JAXBElement<CrearCuenta>(_CrearCuenta_QNAME, CrearCuenta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Pagos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "pagos")
    public JAXBElement<Pagos> createPagos(Pagos value) {
        return new JAXBElement<Pagos>(_Pagos_QNAME, Pagos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NuevoCreditoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "nuevoCreditoResponse")
    public JAXBElement<NuevoCreditoResponse> createNuevoCreditoResponse(NuevoCreditoResponse value) {
        return new JAXBElement<NuevoCreditoResponse>(_NuevoCreditoResponse_QNAME, NuevoCreditoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MisCreditos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "misCreditos")
    public JAXBElement<MisCreditos> createMisCreditos(MisCreditos value) {
        return new JAXBElement<MisCreditos>(_MisCreditos_QNAME, MisCreditos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearCuentaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "crearCuentaResponse")
    public JAXBElement<CrearCuentaResponse> createCrearCuentaResponse(CrearCuentaResponse value) {
        return new JAXBElement<CrearCuentaResponse>(_CrearCuentaResponse_QNAME, CrearCuentaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "pagosResponse")
    public JAXBElement<PagosResponse> createPagosResponse(PagosResponse value) {
        return new JAXBElement<PagosResponse>(_PagosResponse_QNAME, PagosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NuevoCredito }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "nuevoCredito")
    public JAXBElement<NuevoCredito> createNuevoCredito(NuevoCredito value) {
        return new JAXBElement<NuevoCredito>(_NuevoCredito_QNAME, NuevoCredito.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MisCreditosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "misCreditosResponse")
    public JAXBElement<MisCreditosResponse> createMisCreditosResponse(MisCreditosResponse value) {
        return new JAXBElement<MisCreditosResponse>(_MisCreditosResponse_QNAME, MisCreditosResponse.class, null, value);
    }

}
