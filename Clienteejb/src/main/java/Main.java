import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.InitialContext;

import testejb.OperacionesRemote;


public class Main {
	
	private OperacionesRemote operaciones;
	
	public void conectar() throws Exception {
		try {  
            final Hashtable<String, Comparable> jndiProperties =  
                    new Hashtable<String, Comparable>();  
            jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY,  
                    "org.wildfly.naming.client.WildFlyInitialContextFactory");  
            jndiProperties.put("jboss.naming.client.ejb.context", true);  
              
            jndiProperties.put(Context.PROVIDER_URL, "http-remoting://localhost:8080");  
            jndiProperties.put(Context.SECURITY_PRINCIPAL, "ejb");  
            jndiProperties.put(Context.SECURITY_CREDENTIALS, "241996cuenca");  
              
            final Context context = new InitialContext(jndiProperties);  
              
            final String lookupName = "ejb:/testejb/Operaciones!testejb.OperacionesRemote";  
              
            this.operaciones = (OperacionesRemote) context.lookup(lookupName);  
              
        } catch (Exception ex) {  
            ex.printStackTrace();  
            throw ex;  
        }  
	}
	
	public void testSuma() {
		int res = this.operaciones.suma(10, 10);
		System.out.println(res);
	}

	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Main main = new Main();
		try {
			main.conectar();
			main.testSuma();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
