package vista;



import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;

import javax.ws.rs.core.Response;

import modelo.Transaccion;


@ManagedBean
public class RecargaMB {

	private Transaccion transaccion;
	private String telefono;
	private Double monto;

	@PostConstruct
	public void init() {
		this.transaccion = new Transaccion();
	}
	
	
	
	
	public String nuevaRecarga() {
		String url = "http://localhost:8080/TiendaRecargas/service/cliente/recargar";
		Client cl = ClientBuilder.newClient();
		WebTarget tar = cl.target(url);
		Transaccion t = new Transaccion();
		t.setMonto(this.monto);
		t.setTelefono(this.telefono);
		Response res = tar.request().post(Entity.json(t));
		System.out.println("RESPUESTA DEL SERVIDOR: "+res.toString());
		cl.close();
		return "";
	}

	public Transaccion getTransaccion() {
		return transaccion;
	}

	public void setTransaccion(Transaccion transaccion) {
		this.transaccion = transaccion;
	}




	public String getTelefono() {
		return telefono;
	}




	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}




	public Double getMonto() {
		return monto;
	}




	public void setMonto(Double monto) {
		this.monto = monto;
	}

	
}
