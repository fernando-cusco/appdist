package modelo;


import java.util.List;




public class Cliente {

	
	private int id;

	
	private List<Recarga> recargas;

	private String cedula;

	private String nombres;

	private String telefono;

	private Double saldo;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public List<Recarga> getRecargas() {
		return recargas;
	}

	public void setRecargas(List<Recarga> recargas) {
		this.recargas = recargas;
	}

	@Override
	public String toString() {
		return "Cliente [id=" + id + ", recargas=" + recargas + ", cedula=" + cedula + ", nombres=" + nombres
				+ ", telefono=" + telefono + ", saldo=" + saldo + "]";
	}

}
