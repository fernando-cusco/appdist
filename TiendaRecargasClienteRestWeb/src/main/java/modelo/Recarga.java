package modelo;

import java.util.Date;




public class Recarga {

	
	private int id;
	
	private String telefono;
	
	private Double monto;
	
	private Date fecha;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Override
	public String toString() {
		return "Recarga [id=" + id + ", telefono=" + telefono + ", monto=" + monto + ", fecha=" + fecha + "]";
	}

	
}
