package ec.edu.ups.appdis.clientesoaptest;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 2.7.2
 * 2020-01-06T17:43:58.449-05:00
 * Generated source version: 2.7.2
 * 
 */
@WebService(targetNamespace = "http://servicios/", name = "LibrosServiceSoap")
@XmlSeeAlso({ObjectFactory.class})
public interface LibrosServiceSoap {

    @WebMethod
    @RequestWrapper(localName = "createLibro", targetNamespace = "http://servicios/", className = "ec.edu.ups.appdis.clientesoaptest.CreateLibro")
    @ResponseWrapper(localName = "createLibroResponse", targetNamespace = "http://servicios/", className = "ec.edu.ups.appdis.clientesoaptest.CreateLibroResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ec.edu.ups.appdis.clientesoaptest.Respuesta createLibro(
        @WebParam(name = "arg0", targetNamespace = "")
        ec.edu.ups.appdis.clientesoaptest.Libro arg0
    );

    @WebMethod
    @RequestWrapper(localName = "getLibros", targetNamespace = "http://servicios/", className = "ec.edu.ups.appdis.clientesoaptest.GetLibros")
    @ResponseWrapper(localName = "getLibrosResponse", targetNamespace = "http://servicios/", className = "ec.edu.ups.appdis.clientesoaptest.GetLibrosResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ec.edu.ups.appdis.clientesoaptest.Libro> getLibros();
}
