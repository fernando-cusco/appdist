
package ec.edu.ups.appdis.clientesoaptest;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ec.edu.ups.appdis.clientesoaptest package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetLibrosResponse_QNAME = new QName("http://servicios/", "getLibrosResponse");
    private final static QName _GetLibros_QNAME = new QName("http://servicios/", "getLibros");
    private final static QName _CreateLibro_QNAME = new QName("http://servicios/", "createLibro");
    private final static QName _CreateLibroResponse_QNAME = new QName("http://servicios/", "createLibroResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ec.edu.ups.appdis.clientesoaptest
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateLibro }
     * 
     */
    public CreateLibro createCreateLibro() {
        return new CreateLibro();
    }

    /**
     * Create an instance of {@link CreateLibroResponse }
     * 
     */
    public CreateLibroResponse createCreateLibroResponse() {
        return new CreateLibroResponse();
    }

    /**
     * Create an instance of {@link GetLibrosResponse }
     * 
     */
    public GetLibrosResponse createGetLibrosResponse() {
        return new GetLibrosResponse();
    }

    /**
     * Create an instance of {@link GetLibros }
     * 
     */
    public GetLibros createGetLibros() {
        return new GetLibros();
    }

    /**
     * Create an instance of {@link Libro }
     * 
     */
    public Libro createLibro() {
        return new Libro();
    }

    /**
     * Create an instance of {@link Respuesta }
     * 
     */
    public Respuesta createRespuesta() {
        return new Respuesta();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLibrosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servicios/", name = "getLibrosResponse")
    public JAXBElement<GetLibrosResponse> createGetLibrosResponse(GetLibrosResponse value) {
        return new JAXBElement<GetLibrosResponse>(_GetLibrosResponse_QNAME, GetLibrosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLibros }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servicios/", name = "getLibros")
    public JAXBElement<GetLibros> createGetLibros(GetLibros value) {
        return new JAXBElement<GetLibros>(_GetLibros_QNAME, GetLibros.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateLibro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servicios/", name = "createLibro")
    public JAXBElement<CreateLibro> createCreateLibro(CreateLibro value) {
        return new JAXBElement<CreateLibro>(_CreateLibro_QNAME, CreateLibro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateLibroResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servicios/", name = "createLibroResponse")
    public JAXBElement<CreateLibroResponse> createCreateLibroResponse(CreateLibroResponse value) {
        return new JAXBElement<CreateLibroResponse>(_CreateLibroResponse_QNAME, CreateLibroResponse.class, null, value);
    }

}
