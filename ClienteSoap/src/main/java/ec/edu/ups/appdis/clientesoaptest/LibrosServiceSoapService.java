package ec.edu.ups.appdis.clientesoaptest;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 2.7.2
 * 2020-01-06T17:43:58.470-05:00
 * Generated source version: 2.7.2
 * 
 */
@WebServiceClient(name = "LibrosServiceSoapService", 
                  wsdlLocation = "/var/folders/5d/hlv0kfms7fq9fkw18sdt02n80000gn/T/tempdir3353233700541123599.tmp/LibrosServiceSoap_1.wsdl",
                  targetNamespace = "http://servicios/") 
public class LibrosServiceSoapService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://servicios/", "LibrosServiceSoapService");
    public final static QName LibrosServiceSoapPort = new QName("http://servicios/", "LibrosServiceSoapPort");
    static {
        URL url = LibrosServiceSoapService.class.getResource("/var/folders/5d/hlv0kfms7fq9fkw18sdt02n80000gn/T/tempdir3353233700541123599.tmp/LibrosServiceSoap_1.wsdl");
        if (url == null) {
            java.util.logging.Logger.getLogger(LibrosServiceSoapService.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "/var/folders/5d/hlv0kfms7fq9fkw18sdt02n80000gn/T/tempdir3353233700541123599.tmp/LibrosServiceSoap_1.wsdl");
        }       
        WSDL_LOCATION = url;
    }

    public LibrosServiceSoapService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public LibrosServiceSoapService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public LibrosServiceSoapService() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public LibrosServiceSoapService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public LibrosServiceSoapService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public LibrosServiceSoapService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     *
     * @return
     *     returns LibrosServiceSoap
     */
    @WebEndpoint(name = "LibrosServiceSoapPort")
    public LibrosServiceSoap getLibrosServiceSoapPort() {
        return super.getPort(LibrosServiceSoapPort, LibrosServiceSoap.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns LibrosServiceSoap
     */
    @WebEndpoint(name = "LibrosServiceSoapPort")
    public LibrosServiceSoap getLibrosServiceSoapPort(WebServiceFeature... features) {
        return super.getPort(LibrosServiceSoapPort, LibrosServiceSoap.class, features);
    }

}
