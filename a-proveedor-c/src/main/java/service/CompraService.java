package service;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import modelo.Respuesta;
import modelo.Vuelo;
import negocio.CompraON;

@Path("compra")
public class CompraService {

	@Inject
	private CompraON on;

	@POST
	@Path("/comprar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Respuesta comprar(Vuelo vuelo) {
		return on.nuevaCompra(vuelo);
	}
}
