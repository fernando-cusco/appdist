package service;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import modelo.Respuesta;
import modelo.Vuelo;
import negocio.CompraON;

@WebService
public class CompraSoap {

	@Inject
	private CompraON on;
	
	
	@WebMethod
	public Respuesta comprar(Vuelo vuelo) {
		System.out.println(vuelo.toString());
		return on.nuevaCompra(vuelo);
	}
}
