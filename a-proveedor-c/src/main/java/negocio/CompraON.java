package negocio;

import javax.ejb.Stateless;
import javax.inject.Inject;

import dao.CompraDAO;
import modelo.Compra;
import modelo.Respuesta;
import modelo.Vuelo;

@Stateless
public class CompraON {

	@Inject
	private CompraDAO dao;

	public Respuesta nuevaCompra(Vuelo vuelo) {
		Respuesta respuesta = new Respuesta();
		String msj = "";
		try {
			
			int v = (int)(Math.random()*3+1);
			Compra compra = new Compra();
			String codigo = vuelo.getCodigo();
				
				int descuento = (int)(Math.random()*10+1);
				double valor = 0;
				int porcentaje = descuento * 10;

				if (v == 1) {
					compra.setOrigen("Quito");
					compra.setDestino("Cuenca");
					compra.setCosto(740);
					valor = (740 * (porcentaje * 0.01));
					compra.setDescuento(valor);
					compra.setTotal(740 - valor);
					msj = "Origen: Quito Destino: Cuenca. Costo: 740, Descuento: " + porcentaje + "%, Total: "
							+ compra.getTotal() + ".";

				}
				if (v == 2) {
					compra.setOrigen("Cuenca");
					compra.setDestino("Quito");
					compra.setCosto(670);
					valor = (670 * (porcentaje * 0.01));
					compra.setDescuento(valor);
					compra.setTotal(670 - valor);
					msj = "Origen: Cuenca Destino: Quito. Costo: 670, Descuento: " + porcentaje + "%, Total: "
							+ compra.getTotal() + ".";
				}
				if (v == 3) {
					msj = "Origen: Cuenca Destino: Guayaquil";
					compra.setOrigen("Cuenca");
					compra.setDestino("Guayaquil");
					compra.setCosto(340);
					valor = (340 * (porcentaje * 0.01));
					compra.setDescuento(valor);
					compra.setTotal(340 - valor);
					msj = "Origen: Quito Destino: Cuenca. Costo: 340, Descuento: " + porcentaje + "%, Total: "
							+ compra.getTotal() + ".";
				}
				dao.nuevaCompra(compra);
				respuesta.setCodigo(1);
				respuesta.setMensaje(msj);
			

		} catch (Exception e) {
			respuesta.setCodigo(0);
			respuesta.setMensaje("error " + e.getMessage());
		}
		return respuesta;
	}
}
