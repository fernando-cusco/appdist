package modelo;

public class Vuelo {

	private int vuelo;
	private String codigo;
	public int getVuelo() {
		return vuelo;
	}
	public void setVuelo(int vuelo) {
		this.vuelo = vuelo;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	@Override
	public String toString() {
		return "Vuelo [vuelo=" + vuelo + ", codigo=" + codigo + "]";
	}
	
	
}
