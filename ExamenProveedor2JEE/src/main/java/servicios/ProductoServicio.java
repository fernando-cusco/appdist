package servicios;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import modelos.Producto;
import negocio.ProductoON;
import utils.Pedido;
import utils.Respuesta;

@Path("pedidos")
public class ProductoServicio {

	@Inject
	private ProductoON on;
	
	@POST
	@Path("/pedido")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Respuesta nuevoPedido(Pedido pedido) {
		return on.nuevoPedido(pedido);
	}
	
	@POST
	@Path("/producto")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Respuesta nuevoProducto(Producto producto) {
		return on.nuevoProducto(producto);
	}
	
}
