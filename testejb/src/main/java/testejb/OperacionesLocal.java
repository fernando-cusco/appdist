package testejb;

import javax.ejb.Local;

@Local
public interface OperacionesLocal {
	
	public int suma(int a, int b);

}
