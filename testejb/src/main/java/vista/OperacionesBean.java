package vista;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import testejb.OperacionesLocal;

@ManagedBean									/*enlazar con el html o xhtml, crear como bean  es solo vista*/


public class OperacionesBean {
	
	/*propiedades del bean*/
	private int a;
	
	private int b;
	
	private int r;
	
	
	/*Api cdi de inyeccion y desyeccion*/
	@Inject
	private OperacionesLocal operacionesLocal;
	
	
	public int getA() {
		return a;
	}
	public void setA(int a) {
		this.a = a;
	}
	public int getB() {
		return b;
	}
	public void setB(int b) {
		this.b = b;
	}
	public int getR() {
		return r;
	}
	public void setR(int r) {
		this.r = r;
	}
	
	/*Action Controller metodos para enlazarse con el html*/
	public String calcular() {
		r = operacionesLocal.suma(a, b);
		return null;
	}
	

}

/*ciclo de vida 
 * 1. solicitud
 * 2. la pagina hace una lectura secuencial
 * 3. Identifica 
 * 4. Verifica si hay un ManagedBean
 * 5. Get
 * 
 *  Segunda petiicon con  verifica si no hay errores y llama al action controller 'metodos'
 * */
 

//ambitos de un bean ambito de vida, tiwmpo uqe va a estar en el servidor
//ApplicationScoped		tiempo de vida a nivel de aplicacion, se crea uno a nivel de servidor, y es compartido a los clientes
//SessionScoped			se cre en el server y va a permanecer atado al usuario mientra el usuario este en sesion si cierra el muere/ sesion de cleinte hhtp
//RequestScope SE CREA AL RATO DE LA SOLICITUD Y CUANDO SE RENDERIZA LA PAGINA Y se libera
//ViewScope  es sesion y request es a nivel de vista, permance vivo mientras esta en un pagina si se cambia de pagina se libera, se usa cuando se usa ajax
//ESTOS AMBITOS DE VIDA SE USA PARA LOS RECURSOS ENTRE 1 Y 2 MANABEN POR CLIENTE    View y Request











