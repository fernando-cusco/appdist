
package ec.edu.ups;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ec.edu.ups package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MostarResponse_QNAME = new QName("http://servicios/", "mostarResponse");
    private final static QName _Mostar_QNAME = new QName("http://servicios/", "mostar");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ec.edu.ups
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Mostar }
     * 
     */
    public Mostar createMostar() {
        return new Mostar();
    }

    /**
     * Create an instance of {@link MostarResponse }
     * 
     */
    public MostarResponse createMostarResponse() {
        return new MostarResponse();
    }

    /**
     * Create an instance of {@link Movimiento }
     * 
     */
    public Movimiento createMovimiento() {
        return new Movimiento();
    }

    /**
     * Create an instance of {@link Cuenta }
     * 
     */
    public Cuenta createCuenta() {
        return new Cuenta();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MostarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servicios/", name = "mostarResponse")
    public JAXBElement<MostarResponse> createMostarResponse(MostarResponse value) {
        return new JAXBElement<MostarResponse>(_MostarResponse_QNAME, MostarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Mostar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servicios/", name = "mostar")
    public JAXBElement<Mostar> createMostar(Mostar value) {
        return new JAXBElement<Mostar>(_Mostar_QNAME, Mostar.class, null, value);
    }

}
