package negocio;

import javax.ejb.Stateless;
import javax.inject.Inject;

import dao.VueloDAO;
import modelo.Vuelo;

@Stateless
public class VueloON {

	@Inject
	private VueloDAO dao;
	
	public Vuelo verVuelos(int id) {
		Vuelo v = dao.verVuelos(id);
		return v;
	}
}
