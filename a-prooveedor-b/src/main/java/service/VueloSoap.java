package service;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import modelo.Vuelo;
import negocio.VueloON;

@WebService
public class VueloSoap {

	@Inject
	private VueloON on;
	
	@WebMethod
	public Vuelo nuevoVuelo(Vuelotmp id) {
		int numero = (int)(Math.random()*10+1);
		System.out.println(id.toString());
		return on.verVuelos(numero);
	}
}
