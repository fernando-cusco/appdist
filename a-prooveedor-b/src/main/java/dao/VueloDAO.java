package dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import modelo.Vuelo;

public class VueloDAO {

	@Inject
	private EntityManager em;
	
	public void nuevoVuelo(Vuelo vuelo) {
		em.persist(vuelo);
	}
	
	public Vuelo verVuelos(int id){
		
		String jpql = "select v from Vuelo v where v.id = :id";
		Query query = em.createQuery(jpql, Vuelo.class);
		query.setParameter("id", id);
		Vuelo vuelo = (Vuelo) query.getSingleResult();
		return vuelo;
	}
}
