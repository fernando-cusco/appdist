package vista;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import modelo.Actor;
import modelo.Genero;
import negocio.PeliculasLocal;

@ManagedBean
public class PeliculasView {

	@Inject
	private PeliculasLocal local;

	@Inject
	private FacesContext facesContext;

	/* Bean Properties */
	private Actor actor = new Actor();
	private Genero genero = new Genero();

	private List<Actor> actores;
	private List<Genero> generos;

			
	
	@PostConstruct
	public void init() {
		this.actores = local.listarActores();
		this.generos = local.listarGeneros();
		this.actor.setCodigo(this.local.codigoNextActor());
		this.genero.setCodigo(this.local.codigoNextGenero());
	}

	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public List<Actor> getActores() {
		return actores;
	}

	public void setActores(List<Actor> actores) {
		this.actores = actores;
	}

	public List<Genero> getGeneros() {
		return generos;
	}

	public void setGeneros(List<Genero> generos) {
		this.generos = generos;
	}

	public String crearActor() {
		
		if (local.existeActor(this.actor.getNombres(), this.actor.getNacionalidad())) {
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error, actor ya existe",
					"Error, Actor ya existe");
			facesContext.addMessage(null, m);
		} else {
			this.local.crearActor(this.actor);
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Guardado!", "Exito");
			facesContext.addMessage(null, m);
			
			
			
		}

		return null;
	}

	public String crearGenero() {
		
		if (local.existeGenero(this.genero.getNombre())) {
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error, genero ya existe!",
					"Error, Genero ya existe");
			facesContext.addMessage(null, m);
		} else {
			this.local.crearGenero(this.genero);
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Guardado!", "Exito");
			facesContext.addMessage(null, m);
			
		}

		return "generos";
	}

}
