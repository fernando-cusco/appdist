package negocio;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Stateful;
import javax.ejb.Stateless;

import modelo.Actor;
import modelo.Genero;
import modelo.Pelicula;

@Singleton

public class PeliculasON implements PeliculasLocal, PeliculasRemoto{
	
	List<Actor> listadoActores = new ArrayList<Actor>();
	List<Genero> listadoGeneros = new ArrayList<Genero>();
	
	@Override
	public void crearActor(Actor actor) {
		
		this.listadoActores.add(actor);
	}

	@Override
	public List<Actor> listarActores() {
		return this.listadoActores;
	}

	@Override
	public void crearGenero(Genero genero) {
		this.listadoGeneros.add(genero);
	}

	@Override
	public List<Genero> listarGeneros() {
		return this.listadoGeneros;
	}

	@Override
	public void crearPelicula(Pelicula pelicula) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Pelicula> listarPeliculas() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int codigoNextActor() {
		int size = this.listadoActores.size() + 1;
		return size;
	}

	@Override
	public int codigoNextGenero() {
		int size = this.listadoGeneros.size() + 1;
		return size;
	}

	@Override
	public int codigoNextPelicula() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public boolean existeActor(String nombres, String nacionalidad) {
		boolean flag = false;
		for(int i = 0; i < this.listadoActores.size(); i++) {
			if(this.listadoActores.get(i).getNombres().equals(nombres) && this.listadoActores.get(i).getNacionalidad().equals(nacionalidad)) {
				flag = true;
			}
		}
		return flag;
	}
	public boolean existeGenero(String nombre) {
		boolean flag = false;
		for(int i = 0; i < this.listadoGeneros.size(); i++) {
			if(this.listadoGeneros.get(i).getNombre().equals(nombre)) {
				flag = true;
			}
		}
		return flag;
	}
	
}
