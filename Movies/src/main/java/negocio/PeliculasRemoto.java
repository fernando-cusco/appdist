package negocio;

import java.util.List;

import javax.ejb.Remote;

import modelo.Actor;
import modelo.Genero;
import modelo.Pelicula;

@Remote
public interface PeliculasRemoto {
	
	public void crearActor(Actor actor);
	public List<Actor> listarActores();
	public void crearGenero(Genero genero);
	public List<Genero> listarGeneros();
	public void crearPelicula(Pelicula pelicula);
	public List<Pelicula> listarPeliculas();
	
	public int codigoNextActor();
	public int codigoNextGenero();
	public int codigoNextPelicula();
	
	public boolean existeActor(String nombres, String nacionalidad);
	public boolean existeGenero(String nombre);
}
