package entidad;

public class Producto {
	private int id;
	private int nombres;
	private double precio;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNombres() {
		return nombres;
	}
	public void setNombres(int nombres) {
		this.nombres = nombres;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	
}
