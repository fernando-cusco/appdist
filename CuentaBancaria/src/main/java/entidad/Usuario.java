package entidad;

public class Usuario {
	
	private int id;
	private String cedula;
	private int nombres;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public int getNombres() {
		return nombres;
	}
	public void setNombres(int nombres) {
		this.nombres = nombres;
	}
	
	

}
