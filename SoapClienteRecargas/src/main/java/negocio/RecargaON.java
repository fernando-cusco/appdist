package negocio;

import java.net.URL;


import javax.xml.namespace.QName;

import ec.edu.ups.Cliente;
import ec.edu.ups.ServiceSoap;
import ec.edu.ups.ServiceSoapService;




public class RecargaON {

	private static final QName SERVICE_NAME = new QName("http://webservice/", "ServiceSoapService");
	URL wsdlURL = ServiceSoapService.WSDL_LOCATION;
	ServiceSoapService ss = new ServiceSoapService(wsdlURL, SERVICE_NAME);
    ServiceSoap port = ss.getServiceSoapPort();
    
    
    public Cliente misRecargas(String cedula) {
    	return port.misRecargas(cedula);
    }
}
