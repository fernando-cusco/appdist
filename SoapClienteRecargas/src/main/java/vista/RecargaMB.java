package vista;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import ec.edu.ups.Cliente;
import negocio.RecargaON;

@ManagedBean
public class RecargaMB {

	@Inject
	private RecargaON on;
	
	private Cliente cliente;
	
	private String cedula;

	@PostConstruct
	public void init() {
		this.cliente = new Cliente();
		this.cedula = new String();
	}
	
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	
	
	public String listar() {
		System.out.println("AQUIIIIIIII");
		this.cliente = on.misRecargas(this.cedula);
		return "";
	}
	
	
}
