
package ec.edu.ups;

/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 2.7.2
 * 2020-01-20T10:22:05.204-05:00
 * Generated source version: 2.7.2
 * 
 */
public final class ServiceSoap_ServiceSoapPort_Client {

    private static final QName SERVICE_NAME = new QName("http://webservice/", "ServiceSoapService");

    private ServiceSoap_ServiceSoapPort_Client() {
    }

    public static void main(String args[]) throws java.lang.Exception {
        URL wsdlURL = ServiceSoapService.WSDL_LOCATION;
        if (args.length > 0 && args[0] != null && !"".equals(args[0])) { 
            File wsdlFile = new File(args[0]);
            try {
                if (wsdlFile.exists()) {
                    wsdlURL = wsdlFile.toURI().toURL();
                } else {
                    wsdlURL = new URL(args[0]);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
      
        ServiceSoapService ss = new ServiceSoapService(wsdlURL, SERVICE_NAME);
        ServiceSoap port = ss.getServiceSoapPort();  
        
        {
        System.out.println("Invoking misRecargas...");
        java.lang.String _misRecargas_arg0 = "";
        ec.edu.ups.Cliente _misRecargas__return = port.misRecargas(_misRecargas_arg0);
        System.out.println("misRecargas.result=" + _misRecargas__return);


        }
        {
        System.out.println("Invoking nuevaRecarga...");
        ec.edu.ups.Transaccion _nuevaRecarga_arg0 = null;
        port.nuevaRecarga(_nuevaRecarga_arg0);


        }

        System.exit(0);
    }

}
