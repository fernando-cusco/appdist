
package ec.edu.ups;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ec.edu.ups package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MisRecargas_QNAME = new QName("http://webservice/", "misRecargas");
    private final static QName _MisRecargasResponse_QNAME = new QName("http://webservice/", "misRecargasResponse");
    private final static QName _NuevaRecarga_QNAME = new QName("http://webservice/", "nuevaRecarga");
    private final static QName _NuevaRecargaResponse_QNAME = new QName("http://webservice/", "nuevaRecargaResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ec.edu.ups
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MisRecargas }
     * 
     */
    public MisRecargas createMisRecargas() {
        return new MisRecargas();
    }

    /**
     * Create an instance of {@link MisRecargasResponse }
     * 
     */
    public MisRecargasResponse createMisRecargasResponse() {
        return new MisRecargasResponse();
    }

    /**
     * Create an instance of {@link NuevaRecarga }
     * 
     */
    public NuevaRecarga createNuevaRecarga() {
        return new NuevaRecarga();
    }

    /**
     * Create an instance of {@link NuevaRecargaResponse }
     * 
     */
    public NuevaRecargaResponse createNuevaRecargaResponse() {
        return new NuevaRecargaResponse();
    }

    /**
     * Create an instance of {@link Cliente }
     * 
     */
    public Cliente createCliente() {
        return new Cliente();
    }

    /**
     * Create an instance of {@link Transaccion }
     * 
     */
    public Transaccion createTransaccion() {
        return new Transaccion();
    }

    /**
     * Create an instance of {@link Recarga }
     * 
     */
    public Recarga createRecarga() {
        return new Recarga();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MisRecargas }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "misRecargas")
    public JAXBElement<MisRecargas> createMisRecargas(MisRecargas value) {
        return new JAXBElement<MisRecargas>(_MisRecargas_QNAME, MisRecargas.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MisRecargasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "misRecargasResponse")
    public JAXBElement<MisRecargasResponse> createMisRecargasResponse(MisRecargasResponse value) {
        return new JAXBElement<MisRecargasResponse>(_MisRecargasResponse_QNAME, MisRecargasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NuevaRecarga }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "nuevaRecarga")
    public JAXBElement<NuevaRecarga> createNuevaRecarga(NuevaRecarga value) {
        return new JAXBElement<NuevaRecarga>(_NuevaRecarga_QNAME, NuevaRecarga.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NuevaRecargaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "nuevaRecargaResponse")
    public JAXBElement<NuevaRecargaResponse> createNuevaRecargaResponse(NuevaRecargaResponse value) {
        return new JAXBElement<NuevaRecargaResponse>(_NuevaRecargaResponse_QNAME, NuevaRecargaResponse.class, null, value);
    }

}
