package ec.edu.ups;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 2.7.2
 * 2020-01-20T10:22:05.341-05:00
 * Generated source version: 2.7.2
 * 
 */
@WebService(targetNamespace = "http://webservice/", name = "ServiceSoap")
@XmlSeeAlso({ObjectFactory.class})
public interface ServiceSoap {

    @WebMethod
    @RequestWrapper(localName = "misRecargas", targetNamespace = "http://webservice/", className = "ec.edu.ups.MisRecargas")
    @ResponseWrapper(localName = "misRecargasResponse", targetNamespace = "http://webservice/", className = "ec.edu.ups.MisRecargasResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ec.edu.ups.Cliente misRecargas(
        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0
    );

    @WebMethod
    @RequestWrapper(localName = "nuevaRecarga", targetNamespace = "http://webservice/", className = "ec.edu.ups.NuevaRecarga")
    @ResponseWrapper(localName = "nuevaRecargaResponse", targetNamespace = "http://webservice/", className = "ec.edu.ups.NuevaRecargaResponse")
    public void nuevaRecarga(
        @WebParam(name = "arg0", targetNamespace = "")
        ec.edu.ups.Transaccion arg0
    );
}
