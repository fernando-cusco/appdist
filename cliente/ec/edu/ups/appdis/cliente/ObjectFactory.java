
package ec.edu.ups.appdis.cliente;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ec.edu.ups.appdis.cliente package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TransaccionResponse_QNAME = new QName("http://servicios/", "transaccionResponse");
    private final static QName _Transaccion_QNAME = new QName("http://servicios/", "transaccion");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ec.edu.ups.appdis.cliente
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Transaccion }
     * 
     */
    public Transaccion createTransaccion() {
        return new Transaccion();
    }

    /**
     * Create an instance of {@link TransaccionResponse }
     * 
     */
    public TransaccionResponse createTransaccionResponse() {
        return new TransaccionResponse();
    }

    /**
     * Create an instance of {@link Transferencia }
     * 
     */
    public Transferencia createTransferencia() {
        return new Transferencia();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransaccionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servicios/", name = "transaccionResponse")
    public JAXBElement<TransaccionResponse> createTransaccionResponse(TransaccionResponse value) {
        return new JAXBElement<TransaccionResponse>(_TransaccionResponse_QNAME, TransaccionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Transaccion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servicios/", name = "transaccion")
    public JAXBElement<Transaccion> createTransaccion(Transaccion value) {
        return new JAXBElement<Transaccion>(_Transaccion_QNAME, Transaccion.class, null, value);
    }

}
