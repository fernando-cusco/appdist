
package ec.edu.ups.appdis.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para transferencia complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="transferencia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="monto" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="cDestino" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cOrigen" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transferencia", propOrder = {
    "monto",
    "cDestino",
    "cOrigen"
})
public class Transferencia {

    protected double monto;
    protected int cDestino;
    protected int cOrigen;

    /**
     * Obtiene el valor de la propiedad monto.
     * 
     */
    public double getMonto() {
        return monto;
    }

    /**
     * Define el valor de la propiedad monto.
     * 
     */
    public void setMonto(double value) {
        this.monto = value;
    }

    /**
     * Obtiene el valor de la propiedad cDestino.
     * 
     */
    public int getCDestino() {
        return cDestino;
    }

    /**
     * Define el valor de la propiedad cDestino.
     * 
     */
    public void setCDestino(int value) {
        this.cDestino = value;
    }

    /**
     * Obtiene el valor de la propiedad cOrigen.
     * 
     */
    public int getCOrigen() {
        return cOrigen;
    }

    /**
     * Define el valor de la propiedad cOrigen.
     * 
     */
    public void setCOrigen(int value) {
        this.cOrigen = value;
    }

}
