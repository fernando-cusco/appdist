package modelo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "tbl_persona")
public class Persona {
	
	@Id
	@Column(name = "per_id")
	private int id;
	
	@NotNull
	@Column(name = "per_cedula", length = 10)
	private String cedula;
	
	@NotNull
	@Size(min = 10, max = 40)
	@Column(name = "per_nombres")
	private String nombres;
	
	@NotNull
	@Size(min = 6, max = 25)
	@Column(name = "per_correo")
	private String correo;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "per_fecha_nacimiento")
	private Date fecha;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	@Override
	public String toString() {
		return "Persona [id=" + id + ", cedula=" + cedula + ", nombres=" + nombres + ", correo=" + correo + ", fecha="
				+ fecha + "]";
	}
	
	
}
