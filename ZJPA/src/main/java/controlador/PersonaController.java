package controlador;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import datos.PersonaDao;
import modelo.Persona;

@ManagedBean
public class PersonaController {
	
	@Inject
	private PersonaDao perDao;
	
	private Persona persona;
	
	@PostConstruct
	public void init() {
		persona = new Persona();
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	
	public String guardar() {
		perDao.insertar(persona);
		return null;
	}
}
