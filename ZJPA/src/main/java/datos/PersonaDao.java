package datos;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import modelo.Persona;

@Stateless
public class PersonaDao {

	//jpa gestionado por el servidor
	@Inject
	private EntityManager em;
	
	public void insertar(Persona persona) {
		em.persist(persona);
	}
	
	public void actualizar(Persona persona) {
		em.merge(persona);
	}

	public void borrar(String cedula) {
		em.remove(leer(cedula));
	}
	public Persona leer(String cedula) {
		em.find(Persona.class, cedula);
		return null;
	}
	
}
