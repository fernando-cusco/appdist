package negocio;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import modelo.Actor;
import modelo.Genero;

@Stateless
public class GestionPeliculas {
	
	private List<Actor> actores = new ArrayList<Actor>();
	private List<Genero> generos = new ArrayList<Genero>();
	
	public void crearActor(Actor actor) {
		this.actores.add(actor);
	}
	
	public void crearGenero(Genero genero) {
		this.generos.add(genero);
	}
	
	public List<Actor> listaActores() {
		for (Actor actor : actores) {
			System.out.println(actor.toString());
		}
		return this.actores;
	}
	
	public List<Genero> listaGeneros() {
		for (Genero genero : generos) {
			System.out.println(genero.toString());
		}
		return this.generos;
	}
	
	public int nextCodigoActor() {
		int next = this.actores.size() + 1;
		System.out.println("SIZE DEL ARREGLO DE ACTORES" + next);
		return next;
	}
	
	public int nextCodigoGenero() {
		int next = this.generos.size() + 1;
		System.out.println("SIZE DEL ARREGLO DE GENEROS" + next);
		return next;
	}
	
	public boolean existeActor(String nombre, String nacionalidad) {
		boolean flag = false;
		for(int i = 0; i < this.actores.size(); i++) {
			if(this.actores.get(i).getNombres().equals(nombre) && this.actores.get(i).getNacionalidad().equals(nacionalidad)) {
				flag = true;
			}
		}
		return flag;
	}
	
	public boolean existeGenero(String nombre) {
		boolean flag = false;
		for(int i = 0; i < this.generos.size(); i++) {
			if(this.generos.get(i).getNombre().equals(nombre)) {
				flag = true;
			}
		}
		return flag;
	}
	
	
	
	
}
