package vista;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import modelo.Actor;
import modelo.Genero;
import negocio.GestionPeliculas;

@ManagedBean
public class PeliculaBean {
		
	@Inject
	private GestionPeliculas gestion;
	@Inject
	private FacesContext facesContext;
	
	/*Bean Properties*/
	private Actor actor = new Actor();
	private Genero genero = new Genero();
	private List<Actor> actores;
	private List<Genero> generos;
	
	private String errorExiste;
	
	@PostConstruct
	public void init() {
		this.actores = gestion.listaActores();
		this.generos = gestion.listaGeneros();
		this.actor.setId(gestion.nextCodigoActor());
		this.genero.setId(gestion.nextCodigoGenero());
	}
	
	public String getErrorExiste() {
		return errorExiste;
	}

	public void setErrorExiste(String errorExiste) {
		this.errorExiste = errorExiste;
	}

	public List<Actor> getActores() {
		return actores;
	}
	
	public void setActores(List<Actor> actores) {
		this.actores = actores;
	}

	public List<Genero> getGeneros() {
		return generos;
	}

	public void setGeneros(List<Genero> generos) {
		this.generos = generos;
	}

	public Actor getActor() {
		return actor;
	}
	public void setActor(Actor actor) {
		this.actor = actor;
	}
	public Genero getGenero() {
		return genero;
	}
	public void setGenero(Genero genero) {
		this.genero = genero;
	}
	
	public String crearActor() {
		if(gestion.existeActor(this.actor.getNombres(), this.actor.getNacionalidad())) {
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error, actor ya existe", "Error, Actor ya existe");
	         facesContext.addMessage(null, m);
		} else {
			this.gestion.crearActor(this.actor);
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Guardado!", "Exito");
	        facesContext.addMessage(null, m);
		}
		return "peliculas";
	}
	
	public String crearGenero() {
		if(gestion.existeGenero(this.genero.getNombre())) {
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error, genero ya existe!", "Error, Genero ya existe");
	        facesContext.addMessage(null, m);
		} else {
			this.gestion.crearGenero(this.genero);
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Guardado!", "Exito");
	        facesContext.addMessage(null, m);
		}
		
		return "peliculas";
	}

}
