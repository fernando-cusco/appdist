package negocio;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import modelos.Respuesta;
import modelos.Usuario;
import modelos.Vuelo;

public class ConsumerON {

	String urls = "http://localhost:8081/crear";
	public Respuesta proveedor1NuevoUsuario(Usuario usuario) {

		String url = "http://localhost:8081/crear";
		Client cl = ClientBuilder.newClient();
		WebTarget tar = cl.target(url);
		Response res = tar.request().post(Entity.json(usuario));
		Respuesta r = res.readEntity(Respuesta.class);
		cl.close();
		return r;
		
	}

	public List<Vuelo> proveedor2VerVuelos() {
		String url = "http://localhost:8081/vuelos";
		Client cl = ClientBuilder.newClient();
		WebTarget tar = cl.target(url);
		List<Vuelo> vuelos = tar.request().get(new GenericType<List<Vuelo>>() {});
		return vuelos;
	}

	public Respuesta proveedor3ComprarVuelo(Vuelo vuelo) {
		String url = "http://localhost:8081/comprar";
		Client cl = ClientBuilder.newClient();
		WebTarget tar = cl.target(url);
		Response res = tar.request().post(Entity.json(vuelo));
		Respuesta r = res.readEntity(Respuesta.class);
		cl.close();
		return r;
	}
}
