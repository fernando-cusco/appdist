package vista;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import modelos.Respuesta;
import modelos.Usuario;
import modelos.Vuelo;
import negocio.ConsumerON;

@ManagedBean
public class ConsumerMB {

	@Inject
	private ConsumerON on;

	private List<Vuelo> vuelos;

	private Vuelo vuelo;

	private Usuario usuario;

	private Respuesta respuesta;

	@PostConstruct
	public void init() {
		vuelos = new ArrayList<Vuelo>();
		usuario = new Usuario();
		vuelo = new Vuelo();
		respuesta = new Respuesta();
		//verVuelos();
	}

	public void verVuelos() {
		vuelos = on.proveedor2VerVuelos();
	}
	
	public String seleccionar(int id) {
		vuelo.setVuelo(id);
		return null;
	}

	public String nuevoUsuario() {
		respuesta = on.proveedor1NuevoUsuario(usuario);
		String cod = respuesta.getMensaje().substring(49, 53);
		vuelo.setCodigo(cod);
		verVuelos();
		return null;
	}

	public String nuevCompra() {
		respuesta = on.proveedor3ComprarVuelo(vuelo);
		return null;
	}

	public List<Vuelo> getVuelos() {
		return vuelos;
	}

	public void setVuelos(List<Vuelo> vuelos) {
		this.vuelos = vuelos;
	}

	public Vuelo getVuelo() {
		return vuelo;
	}

	public void setVuelo(Vuelo vuelo) {
		this.vuelo = vuelo;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Respuesta getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(Respuesta respuesta) {
		this.respuesta = respuesta;
	}

}
