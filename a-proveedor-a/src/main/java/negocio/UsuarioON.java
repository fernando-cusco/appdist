package negocio;

import javax.ejb.Stateless;
import javax.inject.Inject;

import dao.UsuarioDAO;
import modelo.Respuesta;
import modelo.Usuario;

@Stateless
public class UsuarioON {

	@Inject
	private UsuarioDAO dao;
	
	public Respuesta guardar(Usuario usuario) {
		Respuesta respuesta = new Respuesta();
		try {
			dao.guardar(usuario);
			int numero = (int)(Math.random()*10+1);
			respuesta.setCodigo(numero);
			respuesta.setMensaje("Tienes un "+(numero*10)+"% de descuento. codigo de descuento: cod"+numero);
		} catch (Exception e) {
			respuesta.setCodigo(0);
			respuesta.setMensaje("error "+e.getMessage());
		}
		System.out.println("respuesta: "+respuesta.toString());
		return respuesta;
	}
}
