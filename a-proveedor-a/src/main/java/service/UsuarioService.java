package service;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import modelo.Respuesta;
import modelo.Usuario;
import negocio.UsuarioON;

@Path("usuario")
public class UsuarioService {

	@Inject
	private UsuarioON on;
	
	@POST
	@Path("nuevo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Respuesta nuevoUsuario(Usuariotmp u) {
		Usuario usuario = new Usuario();
		usuario.setUsuario(u.getUsuario());
		usuario.setPassword("admin");
		return on.guardar(usuario);
	}
}
