package service;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import modelo.Respuesta;
import modelo.Usuario;
import negocio.UsuarioON;

@WebService
public class UsuarioSoap {

	@Inject
	private UsuarioON on;
	
	@WebMethod
	public Respuesta nuevo(Usuariotmp usuario) {
		Usuario u = new Usuario();
		u.setUsuario(usuario.getUsuario());
		u.setPassword("admin");
		return on.guardar(u);
	}
}
