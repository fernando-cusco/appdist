package dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import modelo.Usuario;

public class UsuarioDAO {

	@Inject
	private EntityManager em;
	
	public void guardar(Usuario usuario) {
		em.persist(usuario);
	}
}
